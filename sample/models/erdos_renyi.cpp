

#include <cstdint>
#include <ostream>

#include "models/erdos_renyi.hpp"
#include "graph/graph.hpp"
#include "utils/to_dot.hpp"

#define UNUSED(x) (void)x

int main(int argc, char **argv) {
    UNUSED(argc);
    UNUSED(argv);

    erdos_renyi<undirected_graph<size_t>> er_model { 20, 0.5 };
    to_dot_undirected<undirected_graph<size_t>, std::ostream>(er_model(), std::cout);
}

