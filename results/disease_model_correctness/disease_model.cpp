
#include <iostream>
#include <sstream>
#include <string>
#include <functional>
#include <cmath>
#include <array>

#include "simulation/disease.hpp"
#include "utils/running_stats.hpp"
#include "experimental/clique.hpp"
#include "app/generate_graph.hpp"
#include "graph/ra_graph.hpp"




//TODO: put on a header like app/generate_graph.hpp
// -------------------------------------------- HEADER --------------------------------------------------------------------------------------
//
//

std::string disease_model_usage(void) {
    return std::string {
        "Available models:\n"
        "  si_model                                        num_initial_infected interactions_per_node infection_rate\n"
        "  sir_model                                       num_initial_infected interactions_per_node infection_rate susceptible_rate\n"
        "  sis_model                                       num_initial_infected interactions_per_node infection_rate resistant_rate\n"
        "  sis_random_vaccionation_model                   num_initial_infected interactions_per_node infection_rate resistant_rate immune_fraction\n"
        "  sis_targeted_vaccionation_model                 num_initial_infected interactions_per_node infection_rate resistant_rate immune_fraction\n"
        "  sis_vaccination_without_global_knowledge_model  num_initial_infected interactions_per_node infection_rate resistant_rate start_node_fraction num_leaps\n"
        "  sirs_model                                      num_initial_infected interactions_per_node infection_rate resistant_rate susceptible_rate\n"
    };
}


template <typename Graph, typename Arg1, typename Arg2, typename Arg3>
disease_model<Graph> parse_model_vararg(int argc, char **argv, const Graph &g, std::function<disease_model<Graph>(const Graph &, Arg1, Arg2, Arg3)> model, std::function<void(void)> on_error) {
    if (argc < 3) {
        on_error();
    }

    std::stringstream ss;
    ss << argv[0] << " " << argv[1] << " " << argv[2] ;

    Arg1 arg1;
    Arg2 arg2;
    Arg3 arg3;
    if ((ss >> arg1) && (ss >> arg2) && (ss >>arg3)) {
        return model(g, arg1, arg2, arg3);
    } else {
        on_error();
        exit(1);
    }
}

template <typename Graph, typename Arg1, typename Arg2, typename Arg3, typename Arg4>
disease_model<Graph> parse_model_vararg(int argc, char **argv, const Graph &g, std::function<disease_model<Graph>(const Graph &, Arg1, Arg2, Arg3, Arg4)> model,  std::function<void(void)> on_error) {
    if (argc < 4) {
        on_error();
    }

    std::stringstream ss;
    ss << argv[0] << " " << argv[1] << " " << argv[2] << " " << argv[3];

    Arg1 arg1;
    Arg2 arg2;
    Arg3 arg3;
    Arg4 arg4;
    if ((ss >> arg1) && (ss >> arg2) && (ss >>arg3) && (ss >> arg4)) {
        return model(g, arg1, arg2, arg3, arg4);
    } else {
        on_error();
        exit(1);
    }
}


template <typename Graph, typename Arg1, typename Arg2, typename Arg3, typename Arg4, typename Arg5>
disease_model<Graph> parse_model_vararg(int argc, char **argv, const Graph &g,  std::function<disease_model<Graph>(const Graph &, Arg1, Arg2, Arg3, Arg4, Arg5)> model,std::function<void(void)> on_error) {
    if (argc < 5) {
        on_error();
    }

    std::stringstream ss;
    ss << argv[0] << " " << argv[1] << " " << argv[2] << " " << argv[3] << " " << argv[4];

    Arg1 arg1;
    Arg2 arg2;
    Arg3 arg3;
    Arg4 arg4;
    Arg5 arg5;
    if ((ss >> arg1) && (ss >> arg2) && (ss >>arg3) && (ss >> arg4) && (ss >> arg5)) {
        return model(g, arg1, arg2, arg3, arg4, arg5);
    } else {
        on_error();
        exit(1);
    }
}

template <typename Graph, typename Arg1, typename Arg2, typename Arg3, typename Arg4, typename Arg5, typename Arg6>
disease_model<Graph> parse_model_vararg(int argc, char **argv, const Graph &g,  std::function<disease_model<Graph>(const Graph &, Arg1, Arg2, Arg3, Arg4, Arg5, Arg6)> model,std::function<void(void)> on_error) {
    if (argc < 6) {
        on_error();
    }

    std::stringstream ss;
    ss << argv[0] << " " << argv[1] << " " << argv[2] << " " << argv[3] << " " << argv[4] << " " << argv[5];

    Arg1 arg1;
    Arg2 arg2;
    Arg3 arg3;
    Arg4 arg4;
    Arg5 arg5;
    Arg6 arg6;
    if ((ss >> arg1) && (ss >> arg2) && (ss >>arg3) && (ss >> arg4) && (ss >> arg5) && (ss >> arg6)) {
        return model(g, arg1, arg2, arg3, arg4, arg5, arg6);
    } else {
        on_error();
        exit(1);
    }
}



/* 
 * GEN(a, arg1) generates:
 *   function gen_a(int argc, char **argv, std::function<void(void)> on_error) that forwards the call to gen_arg with the same number of arguments(minus one, the name)
 *
 */
#define GEN(model_name, ...) \
    template<typename Graph> \
    disease_model<Graph> parse_ ## model_name (int argc, char **argv, const Graph &g, std::function<disease_model<Graph>(const Graph &, __VA_ARGS__)> model, std::function<void(void)> on_error) { \
        return parse_model_vararg<Graph, __VA_ARGS__>(argc, argv, g, model, on_error); \
    }

//Just the model name and it's arguments (don't forget to include the model)
GEN(si_model,  size_t, size_t, double)
GEN(sir_model, size_t, size_t, double, double)
GEN(sis_model, size_t, size_t, double, double)
GEN(sis_random_vaccination_model, size_t, size_t, double, double, double)
GEN(sis_targeted_vaccination_model, size_t, size_t, double, double, double)
GEN(sis_vaccination_without_global_knowledge_model, size_t, size_t, double, double, double, size_t)
GEN(sirs_model, size_t, size_t, double, double, double)
#undef GEN

template <typename Graph>
disease_model<Graph> parse_disease_model(int argc, char** argv, const Graph &g, std::function<void(void)> on_error) {

    std::string model(argv[0]);
    --argc, argv++;

    if        (model == "si_model")                                        { return parse_si_model<Graph>(argc, argv, g, si_model<Graph>,  on_error);} 
    else if   (model == "sir_model")                                       { return parse_sir_model<Graph>(argc, argv, g, sir_model<Graph>, on_error);} 
    else if   (model == "sis_model")                                       { return parse_sis_model<Graph>(argc, argv, g, sis_model<Graph>, on_error);} 
    else if   (model == "sis_random_vaccination_model")                    { return parse_sis_random_vaccination_model<Graph>(argc, argv, g, sis_random_vaccination_model<Graph>, on_error);} 
    else if   (model == "sis_targeted_vaccination_model")                  { return parse_sis_targeted_vaccination_model<Graph>(argc, argv, g, sis_targeted_vaccination_model<Graph>, on_error);} 
    else if   (model == "sis_vaccination_without_global_knowledge_model")  { return parse_sis_vaccination_without_global_knowledge_model<Graph>(argc, argv, g, sis_vaccination_without_global_knowledge_model<Graph>, on_error);} 
    else if   (model == "sirs_model")                                      { return parse_sirs_model<Graph>(argc, argv, g, sirs_model<Graph>, on_error);} 
    else                                                                   { on_error(); exit(1);}
}

#define GEN(model_name, ...) \
    template<typename Graph> \
    void parse_ ## model_name (int argc, char **argv, const Graph &g, std::function<void(void)> on_error) { \
        parse_model_vararg<Graph, model_name <Graph>, __VA_ARGS__>(argc, argv, g, on_error); \
    }
#undef GEN



//--------------------------------------------------------- end of header --------------------------------------------------------------------------------------------





const size_t num_status = 4;



void on_error() {
    std::cerr
        << "usage: graph_model num_time_steps num_repetitions disease_model\n"
        << "\n"
        << disease_model_usage()
        << model_usage();
    exit(1);
}

//references to argc and argv so we can modify them
size_t parse_size_t(int &argc, char **&argv) {
    if (argc < 1) {
        on_error();
    }

    std::stringstream ss;
    ss << argv[0];

    --argc;
    ++argv;

    size_t value;
    if (ss >> value) {
        return  value;
    } else {
        on_error();
        exit(1);
    }
}


template<typename Graph>
void do_run(disease_model<Graph> &model, std::array<std::vector<running_stats>, num_status> &status_stats, size_t num_time_steps, size_t node_count) {

    //these values are wrong so the loops runs at least once
    //use these values to stop loops
    double prev_infected = 0, prev_susceptible = 0, prev_resistant = 0, prev_immune = 0;
    double next_infected = 1, next_susceptible = 1, next_resistant = 1, next_immune = 1;
    size_t time_step = 0;
    for ( time_step = 0; time_step < num_time_steps; ++time_step) {
        next_susceptible = (std::count_if(model.infections().begin(), model.infections().end(), [] (node_status status) { return status == node_status::SUSCEPTIBLE; }) / (double) node_count);
        status_stats[0][time_step].feed(next_susceptible);

        next_infected = (std::count_if(model.infections().begin(), model.infections().end(), [] (node_status status) { return status == node_status::INFECTED; }) / (double) node_count);
        status_stats[1][time_step].feed(next_infected);

        next_resistant = (std::count_if(model.infections().begin(), model.infections().end(), [] (node_status status) { return status == node_status::RESISTANT; }) / (double) node_count);
        status_stats[2][time_step].feed(next_resistant);

        next_immune = (std::count_if(model.infections().begin(), model.infections().end(), [] (node_status status) { return status == node_status::IMMUNE; }) / (double) node_count);
        status_stats[3][time_step].feed(next_immune);



        const double _error = 1e-6; //avoid shadow variables
        auto close_to = [] (double x, double y, double error) { return ( std::abs(x-y) < error);};

        if(close_to(prev_susceptible, next_susceptible, _error)
            && close_to(prev_infected, next_infected, _error)
            && close_to(prev_resistant, next_resistant, _error)
            && close_to(prev_immune, next_immune, _error)) {
            ; //the process has converged
        }
        
        model.do_time_step();

        prev_susceptible = next_susceptible;
        prev_infected = next_infected;
        prev_resistant = next_resistant;
        prev_immune = next_immune;
    }

    for (; time_step < num_time_steps; ++time_step) {
        status_stats[0][time_step].feed(next_susceptible);
        status_stats[1][time_step].feed(next_infected);
        status_stats[2][time_step].feed(next_resistant);
        status_stats[3][time_step].feed(next_resistant);
    }

}

template <typename Graph, typename Model>
void on_model_parse(int argc, char **argv, Model m) {
    size_t num_time_steps = parse_size_t(argc, argv);
    size_t num_runs = parse_size_t(argc, argv);

    std::array<std::vector<running_stats>, num_status> status_stats;
    for(size_t status = 0; status < num_status; ++status) {
        status_stats[status] = std::vector<running_stats>(num_time_steps);
    }

    for(size_t run = 0; run < num_runs; ++run) {
        Graph g = m();
        //This works because argc and argv aren't passed by reference
        auto disease_model = parse_disease_model<Graph>(argc, argv, g, on_error);

        do_run(disease_model, status_stats, num_time_steps, g.node_count());
    }

    //print stats
    std::cout << "time_step,"
              << "mean_frac_susceptible,stdev_frac_susceptible,"
              << "mean_frac_infected,stdev_frac_infected," 
              << "mean_frac_resistant,stdev_frac_resistant,"
              << "mean_frac_immune,stdev_frac_immune," << std::endl;
    for(size_t time_step = 0; time_step < num_time_steps; ++time_step) {
        std::cout << time_step << ",";
        for(size_t status = 0; status < num_status; ++status) {
            std::cout << status_stats[status][time_step].mean() << "," << status_stats[status][time_step].stddev() << ",";
        }
        std::cout << std::endl;
    }
}



int main(int argc, char **argv) {
    if (argc < 2) {
        on_error();
    }
    // Skip program name
    --argc, ++argv;

    parse_graph_model<undirected_ra_graph<size_t>>(argc, argv, on_error);
    return 0;
}
