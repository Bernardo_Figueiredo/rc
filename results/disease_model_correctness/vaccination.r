#!/usr/bin/env Rscript

library(ggplot2)

join_txt <- function(prefix, txt , suffix, format) {
    paste(prefix, paste(txt, paste(suffix, format, sep=''), sep=''), sep='')
}

make.graph <- function(filename) {
    dat <- read.csv(filename, header=TRUE)
    filename <- substr(basename(filename), 1, nchar(basename(filename)) - 4)
    svg(filename=paste(filename, '.svg', sep=''), width=14, height=7)

#0.0 0.01 0.02 0.05 0.1 0.15 0.2 0.3 0.4
    cols <- c("g = 0.0"=  "#a6cee3", 
              "g = 0.05"= "#1f78b4",
              "g = 0.1"=  "#b2df8a", 
              "g = 0.15"= "#33a02c",
              "g = 0.2"=  "#fb9a99",
              "g = 0.3"=  "#e31a1c",
              "g = 0.4"=  "#fdbf6f",
              "g = 0.5"=  "#ff7f00",
              "g = 0.6"=  "#cab2d6",
              "g = 0.7"=  "#6a3d9a",
              "g = 0.8"=  "#ffff99"
              )

    df <- data.frame(time=dat$time,
                     mean_000=dat$mean_vac0.0, std_000=dat$std_vac0.0,
#                     mean_001=dat$mean_vac0.01, std_001=dat$std_vac0.01,
#                     mean_002=dat$mean_vac0.02, std_002=dat$std_vac0.02,
                     mean_005=dat$mean_vac0.05, std_005=dat$std_vac0.05,
                     mean_01=dat$mean_vac0.1, std_01=dat$std_vac0.1,
                     mean_015=dat$mean_vac0.15, std_015=dat$std_vac0.15,
                     mean_02=dat$mean_vac0.2, std_02=dat$std_vac0.2,
                     mean_03=dat$mean_vac0.3, std_03=dat$std_vac0.3,
                     mean_04=dat$mean_vac0.4, std_04=dat$std_vac0.4,
                     mean_05=dat$mean_vac0.5, std_05=dat$std_vac0.5,
                     mean_06=dat$mean_vac0.6, std_06=dat$std_vac0.6,
                     mean_07=dat$mean_vac0.7, std_07=dat$std_vac0.7,
                     mean_08=dat$mean_vac0.8, std_08=dat$std_vac0.8
                     )

    g <- ggplot(data=df, aes(x=time))

    g <- g + geom_point(aes(x=time, y=mean_000, colour="g = 0.0")) 
    g <- g + geom_errorbar(width=.15, aes(ymin=mean_000-std_000, ymax=mean_000+std_000, colour="g = 0.0"))

#    g <- g + geom_point(aes(x=time, y=mean_001, colour="g = 0.01")) 
#    g <- g + geom_errorbar(width=.15, aes(ymin=mean_001-std_001, ymax=mean_001+std_001, colour="g = 0.01"))

#    g <- g + geom_point(aes(x=time, y=mean_002, colour="g = 0.02")) 
#    g <- g + geom_errorbar(width=.15, aes(ymin=mean_002-std_002, ymax=mean_002+std_002, colour="g = 0.02"))

    g <- g + geom_point(aes(x=time, y=mean_005, colour="g = 0.05")) 
    g <- g + geom_errorbar(width=.15, aes(ymin=mean_005-std_005, ymax=mean_005+std_005, colour="g = 0.05"))

    g <- g + geom_point(aes(x=time, y=mean_01, colour="g = 0.1")) 
    g <- g + geom_errorbar(width=.15, aes(ymin=mean_01-std_01, ymax=mean_01+std_01, colour="g = 0.1"))

    g <- g + geom_point(aes(x=time, y=mean_015, colour="g = 0.15")) 
    g <- g + geom_errorbar(width=.15, aes(ymin=mean_015-std_015, ymax=mean_015+std_015, colour="g = 0.15"))

    g <- g + geom_point(aes(x=time, y=mean_02, colour="g = 0.2")) 
    g <- g + geom_errorbar(width=.15, aes(ymin=mean_02-std_02, ymax=mean_02+std_02, colour="g = 0.2"))

    g <- g + geom_point(aes(x=time, y=mean_03, colour="g = 0.3")) 
    g <- g + geom_errorbar(width=.15, aes(ymin=mean_03-std_03, ymax=mean_03+std_03, colour="g = 0.3"))

    g <- g + geom_point(aes(x=time, y=mean_04, colour="g = 0.4")) 
    g <- g + geom_errorbar(width=.15, aes(ymin=mean_04-std_04, ymax=mean_04+std_04, colour="g = 0.4"))

    g <- g + geom_point(aes(x=time, y=mean_05, colour="g = 0.5")) 
    g <- g + geom_errorbar(width=.15, aes(ymin=mean_05-std_05, ymax=mean_05+std_05, colour="g = 0.5"))

    g <- g + geom_point(aes(x=time, y=mean_06, colour="g = 0.6")) 
    g <- g + geom_errorbar(width=.15, aes(ymin=mean_06-std_06, ymax=mean_06+std_06, colour="g = 0.6"))

    g <- g + geom_point(aes(x=time, y=mean_07, colour="g = 0.7")) 
    g <- g + geom_errorbar(width=.15, aes(ymin=mean_07-std_07, ymax=mean_07+std_07, colour="g = 0.7"))

    g <- g + geom_point(aes(x=time, y=mean_08, colour="g = 0.8")) 
    g <- g + geom_errorbar(width=.15, aes(ymin=mean_08-std_08, ymax=mean_08+std_08, colour="g = 0.8"))

    g <- g + scale_y_continuous("Fraction Stationary Infected", breaks=c(0.0,  0.05,  0.1,  0.15,  0.2,  0.25,  0.3,  0.35,  0.4,  0.45,  0.5,  0.55,  0.6,  0.65,  0.7,  0.75,  0.8,  0.85,  0.9,  0.95, 1))
    g <- g + theme_bw()
    g <- g + scale_colour_manual(name="",values=cols)
    print(g)

    dev.off()
}

for (fn in list.files(path=".", pattern=".*_X_.*.csv", full.names=FALSE)) {
    print(fn)
    make.graph(fn)
}
warnings()
