#!/bin/bash

set -e
set -u

export dir="$(dirname "$0")"
source "$dir"/common


for graph_name in "${!graphs[@]}"; do
    model_name="si_model"; model_args="$(bc <<< "$node_count / 100") $num_interactions 0.1"; run_model "$model_name" "$model_args" "$graph_name" "${graphs[$graph_name]}"
    model_name="sir_model"; model_args="$(bc <<< "$node_count / 100") $num_interactions 0.2 0.05"; run_model "$model_name" "$model_args" "$graph_name" "${graphs[$graph_name]}"
    model_name="sis_model"; model_args="$(bc <<< "$node_count / 100") $num_interactions 0.2 0.15"; run_model "$model_name" "$model_args" "$graph_name" "${graphs[$graph_name]}"

    #model_name and graph_name can't use X
    declare -A models
    models["sis_random_vaccination_model"]="$num_initial_infected $num_interactions 0.5 1 X"
    models["sis_targeted_vaccination_model"]="$num_initial_infected $num_interactions 0.5 1 X"
    models["sis_vaccination_without_global_knowledge_model"]="$num_initial_infected $num_interactions 0.5 1 X 1"

    for model_name in "${!models[@]}"; do
        export model_name
        export model_args="${models[$model_name]}"
        export graph_name
        export graph_args="${graphs[$graph_name]}"
        vaccination_values="0.0 0.05 0.1 0.15 0.2 0.3 0.4 0.5 0.6 0.7 0.8"
        parallel --no-notice 'model_args="$(sed "s/X/{}/" <<< "$model_args")"; run_model "$model_name" "$model_args" "$graph_name" "$graph_args"' ::: $vaccination_values

        #merge all the files  just generated into a single file to the R code is easier
        _vaccination_file="$(model_file "$model_name" "$model_args" "$graph_name" "$graph_args")" #goes with the X
        echo "" > "$_vaccination_file" #empty file
        header=$(mktemp) #holds the csv header
        echo -n "time," >> "$header"
        is_first=true
        for vaccination_value in $vaccination_values; do
            file="$(model_file "$model_name" "$(sed "s/X/$vaccination_value/" <<< "$model_args")" "$graph_name" "$graph_args")"
            temp_file=$(mktemp)
            if [[ "$is_first" = true ]]; then
                #just adding the time steps
                paste -d"" "$_vaccination_file" <(cut -d"," -f"1" "$file") > "$temp_file"
                cat "$temp_file" > "$_vaccination_file"
                is_first=false
            fi
            paste -d"," "$_vaccination_file" <(cut -d"," -f"4,5" "$file") > "$temp_file"
            cat "$temp_file" > "$_vaccination_file"
            rm "$temp_file"
            echo -n "mean_vac${vaccination_value},std_vac${vaccination_value}," >> "$header"
        done
        echo "" >> "$header" #newline
        assembly_file=$(mktemp)
        cat "$header" > "$assembly_file"
        tail -n +2 "$_vaccination_file" >> "$assembly_file"
        cat "$assembly_file" > "$_vaccination_file"
        rm "$assembly_file"
        rm "$header"
    done
done





"$dir"/model.r
"$dir"/vaccination.r
