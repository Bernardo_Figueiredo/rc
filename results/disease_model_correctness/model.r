#!/usr/bin/env Rscript

library(ggplot2)

make.graph <- function(filename) {
    dat <- read.csv(filename, header=TRUE)
    filename <- substr(basename(filename), 1, nchar(basename(filename)) - 4)
    svg(filename=paste(filename, '.svg', sep=''), width=14, height=7)

    cols <- c("Susceptible"="blue", "Infected"="green", "Expected Infected"="darkgreen", "Resistant"="red", "Immune"="black")

    #      df <- data.frame(degree=dat$node, actual=dat$p, expected=dbinom(dat$node, size=V, prob=p))
    df <- data.frame(time=dat$time_step, sus=dat$mean_frac_susceptible, std_sus=dat$stdev_frac_susceptible,
                     inf=dat$mean_frac_infected, std_inf=dat$stdev_frac_infected,
                     res=dat$mean_frac_resistant, std_res=dat$stdev_frac_resistant,
                     imm=dat$mean_frac_immune, std_imm=dat$stdev_frac_immune )

    g <- ggplot(data=df, aes(x=time))
    g <- g + geom_point(aes(x=time, y=sus, colour="Susceptible")) 
    g <- g + geom_errorbar(width=.15, aes(ymin=sus-std_sus, ymax=sus+std_sus, colour="Susceptible"))
    g <- g + geom_point(aes(x=time, y=inf, colour="Infected"))
    g <- g + geom_errorbar(width=.15, aes(ymin=inf-std_inf, ymax=inf+std_inf, colour="Infected"))
    if (grepl("sir", filename) || grepl("sirs", filename)) { #grep == string contains
        g <- g + geom_point(aes(x=time, y=res, colour="Resistant"))
        g <- g + geom_errorbar(width=.15, aes(ymin=res-std_res, ymax=res+std_res, colour="Resistant"))
    }
    if (grepl("vaccination", filename)) { #grep == string contains
        g <- g + geom_point(aes(x=time, y=imm, colour="Immune"))
        g <- g + geom_errorbar(width=.15, aes(ymin=imm-std_imm, ymax=imm+std_imm, colour="Immune"))
    }

#Expected
    if(grepl("si_model", filename)) {
        parts <- unlist(strsplit(filename, "_"))
        frac_initial_infected <- as.integer(parts[3]) / 50000
        infection_rate <- as.double(parts[5])
        f <<- function(t) (frac_initial_infected * exp(infection_rate * 4 * t)) / ( 1 - frac_initial_infected + (frac_initial_infected * exp(infection_rate * 4 * t)))
        g <- g + stat_function(fun=f, aes(colour="Expected Infected"))

    }

    g <- g + scale_x_continuous("Time")
    g <- g + scale_y_continuous("Fraction in state", breaks=c(0.0,  0.05,  0.1,  0.15,  0.2,  0.25,  0.3,  0.35,  0.4,  0.45,  0.5,  0.55,  0.6,  0.65,  0.7,  0.75,  0.8,  0.85,  0.9,  0.95, 1))
    g <- g + theme_bw()
    g <- g + scale_colour_manual(name="",values=cols)
    print(g)
    dev.off()
}

for (fn in list.files(path=".", pattern="^[^X]*\\.csv$", full.names=FALSE)) {
    print(fn)
    make.graph(fn)
}

warnings()

