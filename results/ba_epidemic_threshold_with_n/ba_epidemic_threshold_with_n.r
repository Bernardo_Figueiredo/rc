#!/usr/bin/env Rscript

library(ggplot2)

make.graph <- function(filename) {
  dat <- read.csv(filename, header=TRUE)
  filename <- substr(basename(filename), 1, nchar(basename(filename)) - 4)
  svg(filename=paste(filename, '.svg', sep=''), width=14, height=7)

  cols <- c("Expected Epidemic Threshold"="red", "Epidemic Threshold"="blue")

  df <- data.frame(node_count=dat$n, threshold=dat$threshold)

  g <- ggplot(data=df, aes(x=node_count))
  g <- g + geom_point(aes(x=node_count, y=threshold, colour="Epidemic Threshold"))

  expected <<- function(num_nodes) { .18 / log(num_nodes)}
  g <- g + stat_function(fun=expected, aes(colour="Expected Epidemic Threshold"))

  g <- g + scale_x_log10("n", breaks=c(100, 1000, 10000, 10000, 300000))
  g <- g + scale_y_continuous("Epidemic Threshold", breaks=c(0.08, 0.09, 0.1, 0.11, 0.12, 0.13, 0.14, 0.15, 0.16, 0.17, 0.18, 0.19, 0.2, 0.21, 0.22, 0.23, 0.24, 0.25, 0.26, 0.27, 0.28, 0.29, 0.3))

  g <- g + theme_bw()
  g <- g + scale_colour_manual(name="",values=cols)
  print(g)

  dev.off()
}


for (fn in list.files(path=".", pattern=".*_N_.*.csv$", full.names=FALSE)) {
    print(fn)
    make.graph(fn)
}
warnings()

