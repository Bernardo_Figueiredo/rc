#!/bin/bash

#to run just change the range and make sure you have disease_model built in the build dir(../../build)

set -e
set -u

export dir="$(dirname "$0")"
get_results="$dir/../epidemic_threshold/get_results"

. "$dir"/../disease_model_correctness/common

#$1 is model file
function model_epidemic_threshold_file {
    __model_file="$1"
    echo "$(cut -d"X" -f1 <<< "$__model_file")"*"$(cut -d"X" -f2 <<< "$__model_file")"
}

#$1 is the model_name
#$2 is the model_args
#$3 is the graph_name
#$4 is the graph_args
#$5 is the range
function do_run {
    export model_name="$1"
    export model_args="$2"
    export graph_name="$3"
    export graph_args="$4"
    range="$5"
    parallel  --no-notice 'model_args="$(sed "s/X/{}/" <<< "$model_args")"; run_model "$model_name" "$model_args" "$graph_name" "$graph_args"' ::: $range
    _model_file="$(model_file "$model_name" "$model_args" "$graph_name" "$graph_args")"
    "$get_results" $(model_epidemic_threshold_file "$_model_file") > "$_model_file"
}

range="$(seq -w 060 5 130)"


# N -> node_count
# X -> varying infection_rate
# Z -> num initial infetec

set -x

export num_repetitions=3

unset graphs
declare -A graphs
graphs["barabasi_albert"]="N 3 2"
node_counts="100 300 500 700 1000 2000 3500 5000 7500 10000 13000 16000 20000 23000 27000 31000 35000 39000 50000 75000 100000 200000 300000 500000 750000"
for graph_name in "${!graphs[@]}"; do

    #0.X infection rate where X is the number given by the range. susceptible rate constant at 0.1
    #model_name and graph_name can't use X

    unset models
    declare -A models
    models["sis_model"]="Z $num_interactions 0.X 1"

    for model_name in "${!models[@]}"; do

        for node_count in $node_counts ; do
            _graph_args="$(sed "s/N/$node_count/" <<< "${graphs["$graph_name"]}")"
            _model_args="$(sed "s/Z/$(bc <<< "$node_count / 100")/" <<< ${models["$model_name"]})"
            do_run "$model_name" "$_model_args" "$graph_name" "$_graph_args" "$range"
        done

        #aggregate all epidemic threshold data for the all graph models for a single vaccination rate
        epidemic_threshold_with_n_file="$(model_file "$model_name" "$_model_args" "$graph_name" "${graphs["$graph_name"]}")"

        echo "" > "$epidemic_threshold_with_n_file" #empty file
        echo "n,threshold" >> "$epidemic_threshold_with_n_file"

        for node_count in $node_counts ; do
            #copy from loop above
            _graph_args="$(sed "s/N/$node_count/" <<< "${graphs["$graph_name"]}")"
            _model_args="$(sed "s/Z/$(bc <<< "$node_count / 100")/" <<< ${models["$model_name"]})"

            file="$(model_file "$model_name" "$_model_args" "$graph_name" "$_graph_args")"

#            positive_count=0
#            if [[ $node_count -lt 1000 ]]; then
#                at_positive_count=3
#            elif [[ $node_count -lt 10000 ]]; then
#                at_positive_count=2
#            else
#                at_positive_count=1
#            fi
            while read line; do
                stationary_infected="$(cut -d"," -f2 <<< "$line")"
                if [[ "$(bc -l <<< " $stationary_infected > 0.001")" -eq 1 ]]; then
                    threshold="$(cut -d"," -f1 <<< "$line")"
                    echo "$node_count,$threshold" >> "$epidemic_threshold_with_n_file"
                    break # break from while
                fi
#                stationary_infected="$(cut -d"," -f2 <<< "$line")"
#                if [[ "$(bc -l <<< " $stationary_infected > 0")" -eq 1 ]]; then
#                    positive_count=$((positive_count + 1))
#                fi
#
#                if [[ "$positive_count" -eq "$at_positive_count" ]]; then
#                    threshold="$(cut -d"," -f1 <<< "$line")"
#                    echo "$node_count,$threshold" >> "$epidemic_threshold_with_n_file"
#                    break # break from while
#                fi
            done < "$file"

#            if [[ $positive_count -lt $at_positive_count ]]; then
#                echo "Error in '$graph_name' at '$node_count'"
#            fi

        done
    done
done 

"$dir/ba_epidemic_threshold_with_n.r"
