#!/usr/bin/env Rscript

library(ggplot2)

cc <- read.csv("watts_strogatz_average_clustering_coefficient.csv", header=TRUE)
apl <- read.csv("watts_strogatz_average_path_length.csv", header=TRUE)

png(filename="watts_strogatz.png", width=600, height=400)

df <- data.frame(p_cc=cc$key, mean_cc=cc$mean, p_apl=apl$key, mean_apl=apl$mean)

cols <- c("C/C0"="red","L/L0"="blue")

g <- ggplot(data=df, aes(x=p, y=ratio))
g <- g + geom_point(aes(x=p_cc, y=mean_cc, colour="C/C0"))
#g <- g + geom_errorbar(aes(ymin=d, ymax=e, colour="LINE1"), width=0.1, size=.8)
g <- g + geom_point(aes(x=p_apl, y=mean_apl, colour="L/L0"))
g <- g + scale_x_log10(breaks=c(1e-5, 1e-4, 1e-3, 1e-2, 1e-1, 1))
g <- g + scale_fill_discrete(name="lol", labels=c("1", "2"))
g <- g + theme_bw()
g <- g + scale_colour_manual(name="",values=cols)
print(g)

dev.off()

