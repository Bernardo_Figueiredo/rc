#!/usr/bin/env Rscript

library(ggplot2)

make.graph <- function(filename) {
  dat <- read.csv(filename, header=TRUE)
  filename <- substr(basename(filename), 1, nchar(basename(filename)) - 4)
  svg(filename=paste(filename, '.svg', sep=''), width=14, height=7)

  cols <- c("Expected Frequency"="red", "Degree Frequency"="blue")

  limited_f <- function(f, min, max) {f2 = function(k) { y=f(k); y[k>max + 10]=NA; y[k<min -10]=NA;y}}
  df <- data.frame(degree=dat$degree, freq=dat$freq, std=dat$stddev)

  g <- ggplot(data=df, aes(x=degree, y=freq))
  g <- g + geom_point(aes(x=degree, y=freq, colour="Degree Frequency"))
# doesn' work :(  g <- g + geom_linerange(width=.07, aes(ymin=freq-std, ymax=freq+std, colour="Degree Frequency"))

  if (grepl("erdos_renyi", filename)) {
      parts <- unlist(strsplit(filename, "-"))
      er_degree <<- as.integer(parts[2])
      er_p <<- as.numeric(parts[3])
      g <- g + geom_line(aes(y=dbinom(degree, size=er_degree, prob=er_p), colour="Expected Frequency"))
      g <- g + geom_linerange(width=.07, aes(ymin=freq-std, ymax=freq+std, colour="Degree Frequency"))
  } else { #if (grepl("barabasi_albert", filename)) {
      power_law <<- limited_f(function(k) ( (k/2) ** (-3)), min(df[1]), max(df[1]))
      result <<- apply(df[1], 1, power_law)
      g <- g + geom_point(aes(y=result, colour="Expected Frequency"))
  }
#  } else if (grepl("minimal_model", filename) {
#      power_law <<- limited_f(function(k) ( (k/2) ** (-3)), min(df[1]), max(df[1]))
#  }

  if (grepl("minimal_model", filename) || grepl("barabasi_albert", filename)) {
      g <- g + scale_x_log10("Degree")
      g <- g + scale_y_log10("Degree Frequency")
  } else {
      g <- g + scale_x_continuous("Degree", breaks=pretty(df$degree, n=10))
      g <- g + scale_y_continuous("Degree Frequency", breaks=pretty(df$freq, n=10))
  }



  g <- g + theme_bw()
  g <- g + scale_colour_manual(name="",values=cols)
  print(g)

  dev.off()
}


for (fn in list.files(path=".", pattern="\\.csv$", full.names=FALSE)) {
  make.graph(fn)
}
warnings()

