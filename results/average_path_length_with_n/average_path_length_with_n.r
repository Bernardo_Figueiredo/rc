#!/usr/bin/env Rscript

library(ggplot2)

dat <- read.csv("average_path_length_with_n.csv", header=TRUE)
svg(filename="average_path_length_with_n.svg", width=14, height=7)

print(dat$degree)
print(log(dat$degree))
print(sqrt(dat$degree))

df <- data.frame(degree=dat$degree,
                 mean_er=dat$mean_er, std_er=dat$std_er, expected_er=log(dat$degree),
                 mean_cl=dat$mean_cl, std_cl=dat$std_cl, expected_cl=dat$degree,
                 mean_ba=dat$mean_ba, std_ba=dat$std_ba, expected_ba=log(log(dat$degree)), 
                 mean_mm=dat$mean_mm, std_mm=dat$std_mm, expected_mm=log(log(dat$degree)),
                 mean_rl=dat$mean_rl, std_rl=dat$std_rl, expected_rl=sqrt(dat$degree))
cols <- c("Erdos-Renyi"="green",
          "Expected Erdos-Renyi (~ln(n))"="green",
          "Circular Lattice"="red",
          "Expected Circular Lattice (~n)"="red",
          "Barabási-Albert"="orange",
          "Expected Barabási-Albert (~log(log(n)))"="orange",
          "Minimal Model"="yellow",
          "Expected Minimal Model(~log(log(n)))"="yellow",
          "Rectangular Lattice"="blue",
          "Expected Rectangular Lattice (~sqrt(n))"="blue")

g <- ggplot(data=df, aes(x=degree))

g <- g + geom_point(aes(x=degree, y=mean_er, colour="Erdos-Renyi"))
g <- g + geom_linerange(width=.07, aes(ymin=mean_er-std_er, ymax=mean_er+std_er, colour="Erdos-Renyi"))
g <- g + geom_line(aes(x=degree, y=expected_er, colour="Expected Erdos-Renyi (~ln(n))"))

g <- g + geom_point(aes(x=degree, y=mean_cl, colour="Circular Lattice"))
g <- g + geom_linerange(width=.07, aes(ymin=mean_cl-std_cl, ymax=mean_cl+std_cl, colour="Circular Lattice"))
g <- g + geom_line(aes(x=degree, y=expected_cl, colour="Expected Circular Lattice (~n)"))

#g <- g + geom_point(aes(x=degree, y=mean_ba, colour="Barabási-Albert"))
#g <- g + geom_linerange(width=.07, aes(ymin=mean_ba-std_ba, ymax=mean_ba+std_ba, colour="Barabási-Albert"))
#g <- g + geom_line(aes(x=degree, y=expected_ba, colour="Expected Barabási-Albert (~log(log(n)))"))

#g <- g + geom_point(aes(x=degree, y=mean_mm, colour="Minimal Model"))
#g <- g + geom_linerange(width=.07, aes(ymin=mean_mm-std_mm, ymax=mean_mm+std_mm, colour="Minimal Model"))
#g <- g + geom_line(aes(x=degree, y=expected_mm, colour="Expected Minimal Model(~log(log(n)))"))

g <- g + geom_point(aes(x=degree, y=mean_rl, colour="Rectangular Lattice"))
g <- g + geom_linerange(width=.07, aes(ymin=mean_rl-std_rl, ymax=mean_rl+std_rl, colour="Rectangular Lattice"))
g <- g + geom_line(aes(x=degree, y=expected_rl, colour="Expected Rectangular Lattice (~sqrt(n))"))



g <- g + scale_x_log10(breaks=c(1, 10, 100, 1000, 10000))
g <- g + scale_y_log10(breaks=c(1, 10, 100, 1000, 10000))
g <- g + theme_bw()
g <- g + scale_colour_manual(name="",values=cols)
g <- g + xlab("Number of Nodes")
g <- g + ylab("Average Path Length")
print(g)

dev.off()
warnings()
