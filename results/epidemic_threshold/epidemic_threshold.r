#!/usr/bin/env Rscript

library(ggplot2)

make.graph <- function(filename) {
    dat <- read.csv(filename, header=TRUE)
    filename <- substr(basename(filename), 1, nchar(basename(filename)) - 4)
    svg(filename=paste(filename, '.svg', sep=''), width=14, height=7)

    cols <- c("Infection Rate"="black")

    #      df <- data.frame(degree=dat$node, actual=dat$p, expected=dbinom(dat$node, size=V, prob=p))
    df <- data.frame(ratio=dat$ratio, mean=dat$mean, std=dat$std)

    g <- ggplot(data=df, aes(x=ratio))
    g <- g + geom_point(aes(x=ratio, y=mean, colour="Infection Rate")) 
    g <- g + geom_linerange(width=.07, aes(ymin=mean-std, ymax=mean+std, colour="Infection Rate"))
    g <- g + scale_x_continuous("%", breaks=seq(0, 2, by=0.1))
    g <- g + scale_y_continuous("%", breaks=c(0.0,  0.05,  0.1,  0.15,  0.2,  0.25,  0.3,  0.35,  0.4,  0.45,  0.5,  0.55,  0.6,  0.65,  0.7,  0.75,  0.8,  0.85,  0.9,  0.95, 1))
    g <- g + theme_bw()
    g <- g + scale_colour_manual(name="",values=cols)
    print(g)
    dev.off()
}

for (fn in list.files(path=".", pattern="*_0.X_[^M]*.csv", full.names=FALSE)) {
    print(fn)
    make.graph(fn)
}

warnings()

