#!/usr/bin/env Rscript

library(ggplot2)

make.graph <- function(filename) {
    dat <- read.csv(filename, header=TRUE)
    filename <- substr(basename(filename), 1, nchar(basename(filename)) - 4)
    svg(filename=paste(filename, '.svg', sep=''), width=14, height=7)

    cols <- c("Erdos Renyi"="#e41a1c",
              "Circular Lattice"="#377eb8",
              "Barabási-Albert"="#4daf4a",
              "Minimal Model"="#984ea3",
              "Rectangular Lattice"="#ff7f00",
              "Watts-Strogatz"="#a65628"
              )

    #      df <- data.frame(degree=dat$node, actual=dat$p, expected=dbinom(dat$node, size=V, prob=p))
    df <- data.frame(ratio=dat$ratio,
                     mean_er=dat$mean_erdos_renyi, std_er=dat$std_erdos_renyi,
                     mean_cl=dat$mean_circular_lattice, std_cl=dat$std_circular_lattice,
                     mean_ba=dat$mean_barabasi_albert, std_ba=dat$std_barabasi_albert,
                     mean_mm=dat$mean_minimal_model, std_mm=dat$std_minimal_model,
                     mean_rl=dat$mean_rectangular_lattice, std_rl=dat$std_rectangular_lattice,
                     mean_ws=dat$mean_watts_strogatz, std_ws=dat$std_watts_strogatz
                     )

    g <- ggplot(data=df, aes(x=ratio))

    g <- g + geom_point(aes(x=ratio, y=mean_er, colour="Erdos Renyi")) 
    g <- g + geom_linerange(width=.07, aes(ymin=mean_er-std_er, ymax=mean_er+std_er, colour="Erdos Renyi"))

    g <- g + geom_point(aes(x=ratio, y=mean_cl, colour="Circular Lattice")) 
    g <- g + geom_linerange(width=.07, aes(ymin=mean_cl-std_cl, ymax=mean_cl+std_cl, colour="Circular Lattice"))

    g <- g + geom_point(aes(x=ratio, y=mean_ba, colour="Barabási-Albert")) 
    g <- g + geom_linerange(width=.07, aes(ymin=mean_ba-std_ba, ymax=mean_ba+std_ba, colour="Barabási-Albert"))

    g <- g + geom_point(aes(x=ratio, y=mean_mm, colour="Minimal Model")) 
    g <- g + geom_linerange(width=.07, aes(ymin=mean_mm-std_mm, ymax=mean_mm+std_mm, colour="Minimal Model"))

    g <- g + geom_point(aes(x=ratio, y=mean_rl, colour="Rectangular Lattice")) 
    g <- g + geom_linerange(width=.07, aes(ymin=mean_rl-std_rl, ymax=mean_rl+std_rl, colour="Rectangular Lattice"))

    g <- g + geom_point(aes(x=ratio, y=mean_ws, colour="Watts-Strogatz")) 
    g <- g + geom_linerange(width=.07, aes(ymin=mean_ws-std_ws, ymax=mean_ws+std_ws, colour="Watts-Strogatz"))


    g <- g + scale_x_continuous(expression(beta / delta), breaks=seq(0, 2, by=0.1))
    g <- g + scale_y_continuous("Fraction of stationary infected", breaks=c(0.0,  0.05,  0.1,  0.15,  0.2,  0.25,  0.3,  0.35,  0.4,  0.45,  0.5,  0.55,  0.6,  0.65,  0.7,  0.75,  0.8,  0.85,  0.9,  0.95, 1))
    g <- g + theme_bw()
    g <- g + scale_colour_manual(name="",values=cols)
    print(g)
    dev.off()
}


for (fn in list.files(path=".", pattern=".*_M_.*.csv", full.names=FALSE)) {
    print(fn)
    make.graph(fn)
}

warnings()




