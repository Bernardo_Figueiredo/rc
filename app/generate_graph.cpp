// vim: ts=8 sw=4 sts=4 expandtab


#include "app/generate_graph.hpp"
#include "utils/sgf.hpp"
#include "graph/ra_graph.hpp"

#include <iostream>

void on_error() {
    std::cerr
        << "usage: generate_graph MODEL\n"
        << "\n"
        << model_usage();
    exit(1);
}

template <typename Graph, typename Model>
void on_model_parse(int argc, char **argv, Model m) {
    (void) argc; (void) argv; // silence unused parameter
    to_sgf(m(), std::cout);
}

int main(int argc, char **argv) {

    if (argc < 2) {
        on_error();
        return 1;
    }
    // Skip program name
    --argc, ++argv;

    parse_graph_model<undirected_ra_graph<size_t>>(argc, argv, on_error);

    return 0;
}

