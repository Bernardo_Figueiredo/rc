// vim: ts=8 sts=4 sw=4 expandtab

#include "graph/graph.hpp"
#include "graph/csr_graph.hpp"
#include "models/erdos_renyi.hpp"
#include "models/clique.hpp"
#include "stats/clustering_coeff.hpp"
#include "stats/degree_distribution.hpp"
#include "utils/running_stats.hpp"

#include <algorithm>
#include <cassert>
#include <fstream>
#include <iostream>
#include <sstream>

const unsigned int ITERATIONS = 100;
using node_type = unsigned long;

template <typename Graph>
void generate_stats(const std::vector<Graph>& graphs, const std::string& basename) {
    assert(graphs.size() > 0);

    // Assume all graphs have the same node count
    std::vector<running_stats> degree_dist(graphs.front().node_count());
    for (size_t i = 0; i < graphs.size(); ++i) {
        degree_distribution<Graph> dd(graphs[i]);
        for (typename Graph::node_type degree = 0; degree < dd.max_degree(); ++degree) {
            auto k = dd[degree];
            degree_dist[degree].feed(k);
        }
    }

    {
        std::ofstream f(basename + "_deg-dist.csv");
        f << "degree,prob\n";
        for (size_t degree = 0; degree < degree_dist.size(); ++degree) {
            auto mean = degree_dist[degree].mean();
            if (mean != 0.0) {
                f << degree << "," << mean << "\n";
            }
        }
        std::flush(f);
        f.close();
    }
}

int main() {
    using std::to_string;

    std::initializer_list<std::tuple<node_type, double>> all_params = {
        std::make_tuple(100, 0.05),
        std::make_tuple(1000, 0.005),
        std::make_tuple(10000, 0.0005),
        std::make_tuple(100000, 0.00005),
    };

    for (auto params : all_params) {
        node_type num_nodes;
        double edge_probability;
        std::tie(num_nodes, edge_probability) = params;

        std::cout << "Generating Erdős–Rényi(n=" << num_nodes << ", p=" << edge_probability << ")... ";
        std::flush(std::cout);

        std::vector<csr_graph<node_type>> graphs;
        graphs.reserve(ITERATIONS);

        {
            erdos_renyi<undirected_graph<node_type>> model(num_nodes, edge_probability);
            for (unsigned int i = 0; i < ITERATIONS; ++i) {
                if (i % (ITERATIONS/10) == 0) {
                    std::cout << (100*i/ITERATIONS) << "% "; std::flush(std::cout);
                }

                graphs.emplace_back(model());
            }
        }

        std::cout << "100%\n";

        std::string basename;
        {
            std::stringstream ss;
            ss << "er_" << num_nodes << "_" << edge_probability;
            basename = ss.str();
        }
        generate_stats(graphs, basename);
    }

    return 0;
}
