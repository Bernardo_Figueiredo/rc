// vim: ts=8 sts=4 sw=4 expandtab

#include "graph/csr_graph.hpp"
#include "stats/average_clustering_coefficient.hpp"
#include "stats/average_degree.hpp"
#include "stats/average_degree_distribution.hpp"
#include "stats/average_path_length.hpp"
#include "stats/max_degree.hpp"
#include "stats/base_stat.hpp"

#include <cstddef>
#include <cstring>
#include <fstream>
#include <iostream>
#include <memory>

// XXX: since we're using C++11, we don't have std::make_unique,
// so we implement our own...
// Related: https://youtu.be/MAYt6dpCgOI?t=28s
template <typename T, typename... Args>
std::unique_ptr<T> make_unique(Args&& ...args) {
    return std::unique_ptr<T>(new T(std::forward<Args>(args)...));
}

void usage() {
    std::cerr
        << "usage: graph_stat STAT GRAPH...\n"
        << "\n"
        << "All GRAPHs must be in SGF\n"
        << "\n"
        << "Available stats:\n"
        << "  average_clustering_coefficient\n"
        << "  average_path_length\n"
        << "  average_degree\n"
        << "  degree_distribution\n"
        << "  max_degree\n"
        ;
    exit(1);
}

template <typename Graph>
std::unique_ptr<base_stat<Graph>> get_stat(const std::string& input_stat) {
    if (input_stat == "average_clustering_coefficient") {
        return make_unique<average_clustering_coefficient<Graph>>();
    } else if (input_stat == "average_degree") {
        return make_unique<average_degree<Graph>>();
    } else if (input_stat == "average_path_length") {
        return make_unique<average_path_length<Graph>>();
    } else if (input_stat == "degree_distribution") {
        return make_unique<average_degree_distribution<Graph>>();
    } else if (input_stat == "max_degree") {
        return make_unique<max_degree<Graph>>();
    }

    usage();
    abort();
}

using GraphType = csr_graph<size_t>;

//TODO: FIXME: switch better
int main(int argc, char **argv) {
    if (argc < 3) { usage(); abort(); }

    // Skip program name
    --argc, ++argv;

    auto stat = get_stat<GraphType>(argv[0]);

    // Skip stat
    --argc, argv++;

    while (argc > 0) {
        std::ifstream input(argv[0]);
        if (!input || !input.is_open()) {
            std::cerr
                << "Could not open " << argv[0] << ": "
                << strerror(errno) << std::endl;
            continue;
        }

        try {
            std::pair<bool, GraphType> result = csr_graph_from_sgf<size_t>(input);
            if (result.first) {
                stat->feed(result.second);
            } else {
                std::cerr
                    << "Graph not in SGF: " << argv[0] << std::endl;
            }
        } catch (const std::bad_alloc& e) {
            std::cerr << "Could not allocate graph: "
                      << e.what() << std::endl;
        }

        input.close();

        --argc, ++argv;
    }

    stat->output_to(std::cout);
}
