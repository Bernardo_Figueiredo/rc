#include "graph/graph.hpp"
#include "experimental/clique.hpp"
#include "simulation/disease.hpp"

int main() {
    using Graph = typename clique::Graph;

    int graph_size = 1000;
    clique clique_gen(graph_size);
    Graph g = clique_gen();

    size_t num_initial_infected_nodes = 5;
    size_t interactions_per_node = 5;
    double infection_rate = 0.2;

    auto si =  si_model<Graph> (g, num_initial_infected_nodes, interactions_per_node, infection_rate);

    size_t time_steps = 15;
    for (size_t step = 0; step < time_steps; ++step) {
        si.do_time_step();

        size_t aux=0;
        for(auto cur_node_status : si.infections()) {
            if(cur_node_status == node_status::INFECTED) {
                ++aux;
            }
        }

        std::cout << step << ", " << aux*1.0/graph_size << std::endl;

    }
    return 0;
}
