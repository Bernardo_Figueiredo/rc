#!/usr/bin/env Rscript

library(ggplot2)

make.graph <- function(filename) {
  dat <- read.csv(filename, header=TRUE)
  parts <- unlist(strsplit(filename, "_"))
  filename <- paste(filename, '.svg', sep='')
  svg(filename=filename, width=7, height=3)

  if (parts[1] == "er") {
    V <- as.integer(parts[2])
    p <- as.numeric(parts[3])

    dat <- dat[dat$prob > 0,]
    df <- data.frame(degree=dat$degree, actual=dat$prob, expected=dbinom(dat$degree, size=V, prob=p))

    g <- ggplot(data=df, aes(x=degree))
    g <- g + geom_line(aes(y=expected), color="green")
    g <- g + geom_point(aes(x=degree, y=actual))
    g <- g + ylab("probability")
    print(g)
  }

  dev.off()
}

for (fn in list.files(path=".", pattern="\\.csv$", full.names=FALSE)) {
  make.graph(fn)
}
