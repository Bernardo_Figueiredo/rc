// vim: ts=8 sw=4 sts=4 expandtab
#pragma once
#include <cmath>
#include <cstddef>

/*
 * Implementation of the Welford method for computing the standard deviation and the mean.
 * Taken from http://www.johndcook.com/blog/standard_deviation/
 *
 */

class running_stats {
    double m, s;
    size_t k;

public:
    running_stats() : m(0), s(0), k(0) {}

    void feed(double x) {
        k += 1;

        if (k == 1) {
            m = x;
        } else {
            double old_m = m, old_s = s;
            m = old_m + (x - old_m) / double(k);
            s = old_s + (x - old_m) * (x - m);
        }
    }

    size_t count() const { return k; }
    double mean() const { return k > 0 ? m : 0.0; }
    double variance() const { return k > 1 ? s / double(k - 1) : 0.0; }
    double stddev() const { return ::sqrt(variance()); }
};
