#pragma once

#include <string>
#include <iostream>

/*
 * sgf - simple graph format
 *
 * #num nodes
 * #num edges
 * ordered (by first node) edges (one per line)
 * ....
 */

template<typename Graph>
void to_sgf(const Graph& G, std::ostream& os) {
    os << G.node_count() << std::endl;
    if (Graph::directed) {
        os << G.edge_count() << std::endl;
    } else {
        os << (G.edge_count() * 2) << std::endl;
    }
    
    for (typename Graph::node_type node(0); node < G.node_count(); node++) {
        for (const auto& neighbour : G.neighbours_of(node)) {
            os << std::to_string(node) << " " << std::to_string(neighbour) << std::endl;
        }
    }
}
