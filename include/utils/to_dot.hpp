// vim: ts=8 sw=4 sts=4 expandtab
#pragma once
#include <string>

template <typename Graph, typename OutputStream>
void to_dot_undirected(const Graph& G, OutputStream& o) {
    o << "graph G {\n";

    for (typename Graph::node_type node = 0; node < G.node_count(); ++node) {
        o << "  " << node << ";\n";
        for (const auto& neighbour : G.neighbours_of(node)) {
            if (node < neighbour) {
                o << "  " << std::to_string(node) << " -- " << std::to_string(neighbour) << ";\n";
            }
        }
    }

    o << "}";
}

template <typename Graph, typename OutputStream>
void to_dot_directed(const Graph& G, OutputStream& o) {
    o << "digraph G {\n";

    for (typename Graph::node_type node = 0; node < G.node_count(); ++node) {
        o << "  " << node << ";\n";
        for (const auto& neighbour : G.neighbours_of(node)) {
            o << "  " << std::to_string(node) << " -> " << std::to_string(neighbour) << ";\n";
        }
    }

    o << "}";
}
