#pragma once


#ifdef NDEBUG

#define GENERIC_LOG(LOG_TYPE, MSG)
#define LOG_BT(MSG)
#define SHOW_STACKFRAME()
#define LOG_DEBUG(MSG)
#define ASSERT_MSG(EXPR, MSG)
#define ASSERT(EXPR)

#else

#include <iostream>
#include <signal.h>
#include <execinfo.h>

//Check where backtrace is available and include only for those systems
void show_stackframe(void);
#define GENERIC_LOG(LOG_TYPE, MSG) std::cerr << LOG_TYPE << MSG << std::endl
#define FILE_LOCATION_LOG(LOG_TYPE, MSG) GENERIC_LOG(LOG_TYPE, __FILE__ << ":" << __LINE__ << " " << __FUNCTION__ << " " << MSG)
#define LOG_BT(MSG) GENERIC_LOG("[BT]", MSG)

//temporarily defined like this so we don't have to link with assert.o
#define SHOW_STACKFRAME() do {\
    const int BT_LIMIT = 100;\
    void *trace[BT_LIMIT];\
    char **messages = (char **)NULL;\
    int i, trace_size = 0;\
    trace_size = backtrace(trace, BT_LIMIT);\
    messages = backtrace_symbols(trace, trace_size);\
    LOG_BT("Execution_path:");\
    for (i=0; i<trace_size; ++i) {\
        LOG_BT(messages[i]);\
    }\
} while (0)

//Public macros
#define LOG_DEBUG(MSG) FILE_LOCATION_LOG("[DEBUG]", MSG)
#define ASSERT_MSG(EXPR, MSG) do {\
    if (!(EXPR)) {\
        FILE_LOCATION_LOG("[ASSERT]", "Evaluation of '" << #EXPR << "' failed. MSG:" << MSG);\
        SHOW_STACKFRAME();\
        abort();\
    }\
} while (0)

#define ASSERT(EXPR) ASSERT_MSG(EXPR, "")
#endif
