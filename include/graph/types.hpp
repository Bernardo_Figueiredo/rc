// vim: ts=8 sw=4 sts=4 expandtab

#pragma once

/*
 * Type tagging for graphs.
 * static_graphs are graphs which have a fixed representation and don't allow for addition/deletion operations
 *   they are good because it's possible to use less memory than dynamic graphs.
 *   they can only be used for queries (has_edge, etc)
 *
 * dynamic_graphs are graphs which allow addition/deletion operations
 */

struct static_graph {

    /*
     * Must implement:
     *
     * Typedefs:
     * typedef *whatever* node_type;
     *
     * Constructors:
     * *to be decided*
     * 
     * Queries:
     * node_type node_count(void) const;
     * bool has_edge(node_type u, node_type v) const;
     * *Type to be decided* neighbours_of(node_type u) const;
     *
     */

};
struct dynamic_graph : public static_graph {
    /*
     * Must implement the Typedefs and Queries from above.
     *
     * Constructors:
     * graph(node_type node_count);
     *
     * Modifying operations:
     * void add_edge(node_type u, node_type v);
     *
     */
};
