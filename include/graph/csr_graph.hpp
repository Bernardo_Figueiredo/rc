// vim: ts=8 sw=4 sts=4 expandtab
#pragma once

#include "graph/assert.hpp"
#include "graph/graph.hpp"
#include "graph/types.hpp"

#include <algorithm>
#include <istream>
#include <utility>
#include <vector>

/*
 * csr graph representation
 *
 * Notes:
 * Self-loops are not checked.
 *   e.g. (1,1)
 * Duplicate edges are not checked.
 *   e.g. (1,2) (1,2)
 * You must add the edges in order of the from node.
 *   e.g. valid: (1,3) (1,2) (2,1) (2,4)
 *   e.g. not valid: (1,3) (2,1) (1,2)
 *
 * Problems:
 * Due to not being able to add the pair (u,v) and (v,u) at the same time in this format(because we can't guess where the index of v starts),
 * if you want an undirected graph, just add (u,v) and (v,u) manually.
 * If you want an undirected graph the number of edges is two times the number of "conceptual" edges, because all edges are directed
 *   e.g. (1,2) and (2,1) count as two edges insted of one
 */

template<typename NodeType>
class csr_graph : public static_graph {
    private:
        //Indexes the start of edges for node i in _edges
        std::vector<NodeType> _edge_index;
        //Array of all edges of all vertex
        std::vector<NodeType> _edges;
        NodeType _node_count;
        NodeType _edge_count;


        //iterate only until i < _edge_index[u+1] not until <=
        std::pair<NodeType, NodeType> _start_end_index_for(NodeType u) const {
            return std::pair<NodeType, NodeType>{ _edge_index[u], _edge_index[u+1]};
        }

    public:
        using node_type = NodeType;
        using edge_type = std::pair<NodeType, NodeType>;
        static constexpr bool directed = false;

        // We can do this since std::vector is a contiguous container, as
        // required in the standard
        class neighbour_list {
            public:
                typedef typename decltype(_edges)::const_iterator const_iterator;
                typedef typename decltype(_edges)::size_type size_type;

                neighbour_list(const_iterator it, const_iterator et)
                    : _it(it), _et(et) { }

                const_iterator begin() const { return _it; }
                const_iterator end() const { return _et; }
                const_iterator cbegin() const { return _it; }
                const_iterator cend() const { return _et; }
                size_type size() const { return _et - _it; }

            private:
                const_iterator _it;
                const_iterator _et;
        };

        csr_graph(const std::initializer_list<edge_type>& edge_list)
            : csr_graph(0, edge_list) { }

        csr_graph(NodeType node_count, const std::initializer_list<edge_type>& edge_list_)
            : _edge_index{0} // _edge_index is a vector with a 0
            , _edges{}
            , _node_count{0}
            , _edge_count{0}
        {
            std::vector<edge_type> edge_list(edge_list_.begin(), edge_list_.end());
            std::sort(edge_list.begin(), edge_list.end());

            auto et = std::unique(edge_list.begin(), edge_list.end());
            auto it = edge_list.begin();

            _edge_count = std::distance(it, et);
            if (_edge_count == 0) {
                return;
            }
            _edges.reserve(_edge_count);

            // et is a past-the-end iterator, so we take 1. We add 1 since nodes start from 0.
            _node_count = std::max(node_count, (et-1)->first + 1);
            if (_node_count == 0) {
                return;
            }
            _edge_index.reserve(_node_count+1);

            ASSERT(_node_count >= 0);

            for (NodeType u = 0; u < _node_count; ++u) {
                while (it != et && it->first == u) {
                    _edges.push_back(it->second);
                    ++it;
                }
                _edge_index.push_back(_edges.size());
            }
        }

        template <typename Graph>
        csr_graph(const Graph& G) : _edge_index{0} {
            ASSERT(G.node_count() >= 0);
            ASSERT(G.edge_count() >= 0);

            _node_count = G.node_count();
            _edge_count = G.edge_count() * (Graph::directed ? 1 : 2);

            _edge_index.reserve(node_count() + 1);
            _edges.reserve(edge_count());

            for (typename Graph::node_type u(0); u < G.node_count(); ++u) {
                for (const auto& v : G.neighbours_of(u)) {
                    _edges.push_back(v);
                }

                // We sort from the last index to the end of the current edge array.
                auto sort_start = _edges.begin() + _edge_index.back();
                auto sort_end = _edges.begin() + _edges.size();
                std::sort(sort_start, sort_end);

                _edge_index.push_back(_edges.size());
            }
        }

        NodeType node_count() const {
            return _node_count;
        }

        NodeType edge_count() const {
            return _edge_count;
        }

        bool has_edge(NodeType u, NodeType v) const {
            ASSERT_NODE(u);
            ASSERT_NODE(v);

            // if start_of_edges > end_of_edges then we don't iterate over nothing
            std::pair<NodeType,NodeType> start_end_index {_start_end_index_for(u)};

            return std::binary_search(_edges.begin() + start_end_index.first,
                                      _edges.begin() + start_end_index.second,
                                      v);
        }

        neighbour_list neighbours_of(NodeType u) const {
            //TODO: improved by creating an iterator for csr_graph
            ASSERT_NODE(u);

            std::pair<NodeType,NodeType> start_end_index {_start_end_index_for(u)};
            ASSERT(start_end_index.second >= start_end_index.first);
            //LOG_DEBUG("(" << start_end_index.first << ";" << start_end_index.second << ")");

            return neighbour_list(
                _edges.cbegin() + start_end_index.first,
                _edges.cbegin() + start_end_index.second
            );
        }
};

template<typename NodeType, typename InputStream>
std::pair<bool, csr_graph<NodeType>> csr_graph_from_sgf(InputStream& is) {
    NodeType node_count;
    // FIXME: unused; kept for compatibility
    NodeType edge_count;
    bool works;

    works = bool(is >> node_count);
    if (!works) return { false, { } };

    works = bool(is >> edge_count);
    if (!works) return { false, { } };
    (void)edge_count;

    directed_graph<NodeType> graph(node_count);

    NodeType u, v;
    while (is >> u && is >> v) {
        graph.add_edge(u, v);
    }

    return { true, csr_graph<NodeType>(graph) };
}
