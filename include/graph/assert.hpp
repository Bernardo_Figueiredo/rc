#pragma once

#include "assert/assert.hpp"

#define ASSERT_NODE(u) ASSERT(u >= 0); ASSERT_MSG(u < node_count(), "u:" << u << " node_count():" << node_count())
