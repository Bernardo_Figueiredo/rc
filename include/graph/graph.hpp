// vim: ts=8 sw=4 sts=4 expandtab
#pragma once

#include "graph/assert.hpp"
#include "graph/types.hpp"

#include <vector>
#include <unordered_set>

/// A simple Directed graph
template <typename Node, bool Directed>
class graph : public dynamic_graph {
    std::vector<std::unordered_set<Node>> _edges;
    Node _edge_count;

public:
    typedef Node node_type;
    static constexpr bool directed = Directed;

    graph(Node node_count)
        : _edges(node_count)
        , _edge_count(0)
    {

        ASSERT(node_count >= 0);
    }

    Node node_count() const {
        return _edges.capacity();
    }

    Node edge_count() const {
#if 0 && NDEBUG
        Node real_edge_count(0);
        for (const auto &neighs : _edges) {
            real_edge_count += neighs.size();
        }
        if (!Directed) { real_edge_count /= 2; }
        ASSERT_MSG(real_edge_count == _edge_count, real_edge_count << "-" << _edge_count);
#endif

        return _edge_count;
    }

    void add_edge(Node u, Node v) {
        ASSERT_NODE(u);
        ASSERT_NODE(v);
        ASSERT_MSG( u != v, "Detected self-loop (" << u << "," << v << "). They're not allowed");
        ASSERT_MSG( ! has_edge(u,v), "Detected multi edge (" << u << "," << v << "). They're not allowed");

        // Avoid double counting when the join is made twice
        auto result = _edges[u].insert(v);
        if (result.second) {
            _edge_count++;
        }

        if (!Directed) {
            _edges[v].insert(u);
        }
    }

    bool has_edge(Node u, Node v) const {
        ASSERT_NODE(u);
        ASSERT_NODE(v);

        auto it = _edges[u].find(v);
        auto et = _edges[u].end();

        return it != et;
    }

    const std::unordered_set<Node>& neighbours_of(Node u) const {
        ASSERT_NODE(u);

        return _edges[u];
    }

};

template <typename Node>
using directed_graph = graph<Node, true>;
template <typename Node>
using undirected_graph = graph<Node, false>;

template <typename Node, bool Directed>
constexpr bool graph<Node, Directed>::directed;

