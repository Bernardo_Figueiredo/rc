#pragma once

#include "graph/assert.hpp"
#include "graph/types.hpp"

#include <vector>

/// A simple Directed graph
template <typename Node, bool Directed>
class ra_graph : public dynamic_graph {
    std::vector<std::vector<Node>> _edges;
    Node _edge_count;

public:
    typedef Node node_type;
    static constexpr bool directed = Directed;

    ra_graph(Node node_count)
        : _edges(node_count)
        , _edge_count(0)
    {

        ASSERT(node_count >= 0);
    }

    Node node_count() const {
        return _edges.capacity();
    }

    Node edge_count() const {
        return _edge_count;
    }

    void add_edge(Node u, Node v) {
        ASSERT_NODE(u);
        ASSERT_NODE(v);
        ASSERT_MSG( u != v, "Detected self-loop (" << u << "," << v << "). They're not allowed");
        ASSERT_MSG( ! has_edge(u,v), "Detected multi edge (" << u << "," << v << "). They're not allowed");

        // Avoid double counting when the join is made twice
        _edges[u].push_back(v);
        _edge_count++;

        if (!Directed) {
            _edges[v].push_back(u);
        }
    }

    bool has_edge(Node u, Node v) const {
        ASSERT_NODE(u);
        ASSERT_NODE(v);

        for(auto node : _edges[u]) {
            if (node == v) {
                return true;
            }
        }

        return false;
    }

    const std::vector<Node>& neighbours_of(Node u) const {
        ASSERT_NODE(u);

        return _edges[u];
    }

};

template <typename Node>
using directed_ra_graph = ra_graph<Node, true>;
template <typename Node>
using undirected_ra_graph = ra_graph<Node, false>;

template <typename Node, bool Directed>
constexpr bool ra_graph<Node, Directed>::directed;

