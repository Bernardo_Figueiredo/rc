#pragma once

#include "graph/types.hpp"
#include "graph/assert.hpp"

#include <iterator>


class clique {
    private:
        size_t _clique_size;

        class clique_graph : public static_graph {
            private:
                size_t _clique_size;

                    class num_iterator {
                        private:
                            
                            class _num_iterator : public std::iterator<std::random_access_iterator_tag, size_t> {
                                friend class num_iterator; //to call the protected constructor
                                private:
                                    size_t _cur;
                                    size_t _skip;
                                protected:
                                    _num_iterator (size_t start, size_t skip) : _cur(start), _skip(skip) { if(skip == 0 && _cur == 0) {++_cur;}} //otherwise skip=0 would do nothing

                                public:
                                    size_t operator *() const {return _cur;}
                                    const _num_iterator &operator ++() { ++_cur; if(_cur == _skip) { ++_cur; }; return *this;}
                                    const _num_iterator &operator --() { --_cur; if (_cur == _skip) { --_cur;}; return *this;}
                                    size_t operator [] (size_t n) const {
                                        if (_cur < _skip) {
                                            if (_cur + n < _skip) { // n positive
                                                return _cur + n;
                                            } else {
                                                return _cur + n + 1;
                                            }
                                        } else {
                                            if (_cur + n < _skip) { // n negative
                                                return _cur + n - 1;
                                            } else {
                                                return _cur + n;
                                            }
                                        }

                                    }

                                    bool operator ==(const _num_iterator &other) const { return _cur == other._cur && _skip == other._skip;}
                                    bool operator !=(const _num_iterator &other) const { return !(*this == other);} 
                            };

                        private:
                            _num_iterator  _begin;
                            _num_iterator _end;

                        public:
                            num_iterator(size_t max, size_t skip) :  _begin(0, skip), _end(max, skip)  {}

                            const _num_iterator &begin() const { return _begin; }
                            const _num_iterator &end() const {return _end;}
                            const _num_iterator &cbegin() const { return _begin; }
                            const _num_iterator &cend() const {return _end;}

                            size_t operator [] (size_t n) const {return _begin[n];}
                            size_t size() const {return _end._cur - 1; }
                    };
            public:
                typedef size_t node_type;
                static constexpr bool directed = false;


                clique_graph(size_t clique_size) : _clique_size(clique_size) {}

                size_t node_count() const { return _clique_size;}
                size_t edge_count() const { return (_clique_size / 2 ) * (_clique_size - 1); }
                size_t has_edge(size_t u, size_t v) const { return u != v;}
                const num_iterator neighbours_of(size_t u) const { return num_iterator(_clique_size, u);} //iterator from 0 to _clique_size -1 skipping u

        };

    public:
        typedef clique_graph Graph;

        clique(size_t clique_size) : _clique_size(clique_size) {}


        clique_graph operator()() const {
            return clique_graph(_clique_size);
        }

};
