// vim: ts=8 sw=4 sts=4 expandtab
#pragma once
#include "stats/base_stat.hpp"
#include "utils/running_stats.hpp"

template <typename Graph>
class max_degree : public base_stat<Graph> {
    running_stats _stats;

public:
    using node_type = typename Graph::node_type;

    void feed(const Graph& g) override {
        node_type max_deg = 0;
        for (node_type u(0); u < g.node_count(); ++u) {
            max_deg = std::max(max_deg, g.neighbours_of(u).size());
        }

        _stats.feed(double(max_deg));
    }

    void output_to(std::ostream& o) override {
        o << "mean,stddev\n"
          << _stats.mean() << "," << _stats.stddev() << '\n'
          ;
    }
};

