// vim: ts=8 sts=4 sw=4 expandtab
#pragma once
#include "stats/base_stat.hpp"
#include "stats/degree_distribution.hpp"
#include "utils/running_stats.hpp"
#include <unordered_map>

template <typename Graph>
class average_degree_distribution : public base_stat<Graph> {
public:
    using node_type = typename Graph::node_type;

    void feed(const Graph& g) override {
        ++_feed_count;
        degree_distribution<Graph> deg_dist(g);
        // XXX: degree_distribution::operator[] returns 0 if the degree has no
        // representation on the graph
        // XXX: all degrees have a key in _avg_dist, so we can use
        // std::unordered_map::size(). Efficient, but ugly.

        // We need to add 1 to max_degree since we are comparing with <
        // size() is the largest maximum degree we've seen, plus 1.
        // e.g. if we've seen a degree 5, then we have keys {0,1,2,3,4,5}, so size() is 6.
        node_type max_degree = std::max(deg_dist.max_degree() + 1, _avg_dist.size());
        for (node_type u(0); u < max_degree; ++u) {
            _avg_dist[u].feed(deg_dist[u]);
        }
    }

    void output_to(std::ostream& o) override {
        o << "degree,freq,stddev\n";
        for (auto& kv : _avg_dist) {
            auto freq = kv.second.mean();
            if (freq != 0) {
                while (kv.second.count() < _feed_count)
                    kv.second.feed(0);
                o << kv.first << ',' << freq << ',' << kv.second.stddev() << '\n';
            }
        }
    }

private:
    std::unordered_map<node_type, running_stats> _avg_dist;
    size_t _feed_count = 0;
};

