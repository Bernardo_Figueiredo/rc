// vim: sw=4 sts=4 ts=8 expandtab

#pragma once

#include "algo/bfs.hpp"
#include "stats/base_stat.hpp"
#include "utils/running_stats.hpp"
#include <cstddef>

template <typename Graph>
class average_path_length : public base_stat<Graph> {
    running_stats _stats;

public:
    void feed(const Graph& graph) override {
        size_t total_path_length {0};
        size_t num_paths {0};
        bfs<Graph> bfs {graph};

        for(typename Graph::node_type start_node(0); start_node < graph.node_count(); start_node++) {
            bfs.visit(start_node);
            for(auto path_length: bfs.distances()) {
                if (path_length != 0) { // distance being 0 means the node is unreachable
                    total_path_length += path_length;
                    num_paths++;
                }
            }
        }

        _stats.feed(num_paths == 0 ? 0 : double(total_path_length)/double(num_paths));
    }

    double value() const {
        return _stats.mean();
    }

    void output_to(std::ostream& o) override {
        o << "mean,stddev\n"
          << _stats.mean() << ',' << _stats.stddev() << '\n'
          ;
    }
};
