// vim: sw=4 sts=4 ts=8 expandtab
#pragma once
#include "stats/base_stat.hpp"
#include "utils/running_stats.hpp"
#include <cassert>
#include <vector>

template <typename Graph>
class average_degree : public base_stat<Graph> {
    running_stats _stats;

public:
    void feed(const Graph& G) override {
        assert(G.node_count() > 0);

        double avg(0);
        for (typename Graph::node_type u(0); u < G.node_count(); ++u) {
            avg += double(G.neighbours_of(u).size());
        }

        _stats.feed(avg / double(G.node_count()));
    }

    void output_to(std::ostream& o) override {
        o << "mean,stddev\n"
          << _stats.mean() << ',' << _stats.stddev() << '\n'
          ;
    }
};

