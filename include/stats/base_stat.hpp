// vim: ts=8 sw=4 sts=4 expandtab
#pragma once

#include <iostream>

template <typename Graph>
class base_stat {
public:
    typedef Graph graph_type;

    virtual void feed(const Graph& g) = 0;
    virtual void output_to(std::ostream& o) = 0;

    virtual ~base_stat() { }
};

