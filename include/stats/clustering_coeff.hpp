// vim: ts=8 sw=4 sts=4 expandtab
#pragma once
#include <vector>
#include <unordered_set>

template <typename Graph>
class clustering_coeff {
public:
    using node_type = typename Graph::node_type;

    clustering_coeff(const Graph& G) {
        _clustering.reserve(G.node_count());
        for (node_type u(0); u < G.node_count(); ++u) {
            const auto& neighbours = G.neighbours_of(u);
            node_type count(0);

            if (neighbours.size() < 2) {
                _clustering.push_back(0.0);
                continue;
            }

            for (const auto &v : neighbours) {
                for (const auto &w : neighbours) {
                    if (v != w && G.has_edge(v, w) && (Graph::directed || v < w)) {
                        ++count;
                    }
                }
            }

            _clustering.push_back(double(count) / double(neighbours.size() * (neighbours.size() - 1)));
            if (!Graph::directed) {
                _clustering.back() *= 2;
            }
        }
    }

    node_type node_count() const {
        return _clustering.size();
    }

    double operator[](node_type u) const {
        return _clustering[u];
    }

private:
    std::vector<double> _clustering;
};
