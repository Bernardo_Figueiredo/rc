// vim: ts=8 sw=4 sts=4 expandtab
#pragma once
#include "stats/clustering_coeff.hpp"
#include "stats/base_stat.hpp"
#include "utils/running_stats.hpp"
#include <unordered_map>

template<typename Graph>
class average_clustering_coefficient : public base_stat<Graph> {
    running_stats _stats;

public:
    using node_type = typename Graph::node_type;

    void feed(const Graph& graph) override {
        running_stats stats;
        auto ci = clustering_coeff<Graph>(graph);

        for (node_type u(0); u < graph.node_count(); ++u) {
            stats.feed(ci[u]);
        }

        _stats.feed(stats.mean());
    }

    void output_to(std::ostream& o) override {
        o << "mean,stddev\n"
          << value() << ',' << stddev() << '\n'
          ;
    }

    double value() const {
        return _stats.mean();
    }

    double stddev() const {
        return _stats.stddev();
    }

    double variance() const {
        return _stats.variance();
    }
};

