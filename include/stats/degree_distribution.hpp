// vim: ts=8 sw=4 sts=4 expandtab
#pragma once
#include <algorithm>
#include <unordered_map>

template <typename Graph>
class degree_distribution {
    using node_type = typename Graph::node_type;
    std::unordered_map<node_type, double> _dist;
    node_type _node_count;
    node_type _max_degree;

public:
    degree_distribution(const Graph& G)
        : _node_count(G.node_count())
        , _max_degree(0)
    {
        std::unordered_map<node_type, node_type> _degree;
        for (node_type u(0); u < G.node_count(); ++u) {
            node_type degree(G.neighbours_of(u).size());
            ++_degree[degree];
            _max_degree = std::max(_max_degree, degree);
        }

        for (auto p : _degree) {
            _dist[p.first] = double(p.second) / double(_node_count);
        }
    }

    node_type node_count() const { return _node_count; }

    node_type max_degree() const { return _max_degree; }

    double operator[](node_type degree) const {
        // Avoid creating empty slots in hashtable
        auto ret = _dist.find(degree);
        if (ret != _dist.end()) { return ret->second; }
        return 0;
    }
};

