#pragma once

#include <queue>
#include <vector>

template<typename Graph>
class bfs {
    private:
        typedef typename Graph::node_type node_type;

        std::vector<node_type> _distance;
        std::vector<node_type> _parents;
        std::queue<node_type> _pending_visit;
        const Graph &_graph;
    public:

        // () because we don't want to initialize with default constructor for node_*
        bfs(const Graph &graph) : _distance(graph.node_count()), _parents(graph.node_count()), _pending_visit(), _graph{graph} {
            //Not reserving for _pending_visint because we don't know the maximum queue usage
            //Not initializing distance and parents here because we want to initialize it everytime there is visit.
        }

        void visit(const node_type start_node) {
            //TODO: check if node is valid?

            //Initialize containers
            for(node_type node(0); node < _graph.node_count(); node++) { _distance[node] = 0;}
            for(node_type node(0); node < _graph.node_count(); node++) { _parents[node] = node;} //Initially we assume that the node's parent is itself.

            /*
             * This way of implementing the BFS might visit start_node twice, giving it a distance and a parents diffrent than 0 and start_node, respectively.
             * This is harmless because when it's visited nothing is done, because all his neighbours are already visited
             */
            _pending_visit.push(start_node);
            while( ! _pending_visit.empty()) {
                node_type node {_pending_visit.front()};
                _pending_visit.pop();

                for(auto neighbour : _graph.neighbours_of(node)) {
                    if (_distance[neighbour] == 0) {
                        _distance[neighbour] = _distance[node] + 1;
                        _parents[neighbour] = node;
                        _pending_visit.push(neighbour);
                    }
                }
            }
            _parents[start_node] = start_node;
            _distance[start_node] = 0;
        }

        /*
         * If the caller wants, he can copy the vectors himself
         */

        /*
         * For any node except the start_node, if the distance is 0, then it's unreachable
         */
        const std::vector<node_type> &distances() const {
            return _distance;
        }
        
        const std::vector<node_type> &parents() const {
            return _parents;
        }
};
