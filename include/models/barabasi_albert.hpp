#pragma once
#include <iostream>
#include <random>
#include <cmath>

#include "assert/assert.hpp"
#include "models/model.hpp"

template <typename Graph>
struct barabasi_albert : public model<Graph> {
    typedef typename Graph::node_type node_type;

    static_assert(!Graph::directed, "Can't generate the barabasi albert model for directed graphs");


    barabasi_albert(node_type node_count, node_type num_starting_nodes, node_type num_new_edges)
    : _gen(_rd())
    , _uniform_dist(0,1)
    , _node_count(node_count)
    , _num_starting_nodes(num_starting_nodes)
    , _num_new_edges(num_new_edges)
    {
        ASSERT(_node_count >= 0);
        ASSERT(_num_starting_nodes >= 2); //Avoid edges cases
        ASSERT(_num_new_edges >= 0);
        ASSERT(_num_starting_nodes<=_node_count);
        ASSERT(_num_new_edges<=_num_starting_nodes);
    }



    Graph operator()() override {
        // Reseed generator
        _gen.seed(_rd());

        std::vector<node_type> node_degree(_node_count,0);
        Graph g(_node_count);

        //Initially, there's a ring of nodes
        _make_ring_of_nodes(g, node_degree);

        //generate all new nodes
        for(node_type node = _num_starting_nodes+1; node<_node_count; node++){
            _add_edge_round(g, node_degree, node);
        }

        return g;
    }

private:
    static std::random_device _rd;
    std::mt19937 _gen;
    std::uniform_real_distribution<> _uniform_dist;
    node_type _node_count;
    node_type _num_starting_nodes;
    node_type _num_new_edges;

    void _add_edge(Graph &g, std::vector<node_type> &node_degree, node_type u, node_type v) {
        g.add_edge(u,v); //no need to call this->_add_edge because the graph is undirected
        node_degree[u]++;
        node_degree[v]++;
    }


    void _make_ring_of_nodes(Graph &g, std::vector<node_type> &node_degree) {
        //To add a ring we loop until the second to last node, and add the last edge manually
        for (node_type node = 0; node < _num_starting_nodes - 2; node++) {
            _add_edge(g, node_degree, node, node+1);
        }
        _add_edge(g, node_degree, _num_starting_nodes - 1, 0);
    }

    node_type _select_node_by_preferential_attachement(const Graph &g, const std::vector<node_type> &node_degree, const node_type node) {
        node_type index,sum, r;
        do {
            index=floor(_uniform_dist(_gen)*(g.edge_count()*2));
            sum=0;
            r=0;
            while(sum<index){
                sum=sum+node_degree[r];
                r++;
            }
        } while(this->_has_edge_or_self_edge(g, node, r));
        return r;
    }

    void _add_edge_round(Graph &g, std::vector<node_type> &node_degree, node_type node) {

        for(node_type j=1; j<= _num_new_edges;j++){

            _add_edge(g, node_degree, node, _select_node_by_preferential_attachement(g, node_degree, node));
        }
    }


};

template <typename Graph>
std::random_device barabasi_albert<Graph>::_rd;
