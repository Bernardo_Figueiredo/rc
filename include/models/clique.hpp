#pragma once

#include "assert/assert.hpp"
#include "models/model.hpp"

template<typename Graph>
class clique : public model<Graph> {
    private:
        typedef typename Graph::node_type node_type;
        node_type _clique_size;

    public:

    clique(node_type clique_size) : _clique_size(clique_size) {}


    Graph operator()() override {
        ASSERT(_clique_size > 0);

        Graph graph(_clique_size);

        for(node_type node(0); node < _clique_size - 1; node++) { //The last node doesn't have to add edges because they were already added
            for(node_type neighbour(node + 1); neighbour < _clique_size; neighbour++) {
                    graph.add_edge(node, neighbour);
                    if(Graph::directed) {
                        graph.add_edge(neighbour, node);
                    }
            }
        }

        return graph;
    }
};
