// vim: sw=4 sts=4 ts=8 expandtab
#pragma once

#include "assert/assert.hpp"
#include "models/model.hpp"

#include <random>
#include <vector>
#include <unordered_set>

template <typename Graph>
class minimal_model : model<Graph> {
public:
    typedef typename Graph::node_type node_type;

    minimal_model(node_type node_count)
        : _gen(_rd())
        , _node_count(node_count)
    { ASSERT_MSG(node_count >= 2, "node count must be at least 2"); }

    Graph operator()() override {
        _gen.seed(_rd());

        Graph G(_node_count);
        // List of edges for random selection
        std::vector<std::pair<node_type, node_type>> edges;

        // Initial graph: two nodes, connected by one edge
        G.add_edge(0, 1);
        edges.emplace_back(0, 1);

        // At each step pick a random edge and connect the next node
        // to the edge's nodes
        for (node_type next_node(2); next_node < _node_count; ++next_node) {
            std::uniform_int_distribution<size_t> pick_edge(0, edges.size()-1);
            size_t index = pick_edge(_gen);

            auto rand_edge = edges[index];
            G.add_edge(next_node, rand_edge.first);
            G.add_edge(next_node, rand_edge.second);
            edges.emplace_back(next_node, rand_edge.first);
            edges.emplace_back(next_node, rand_edge.second);
        }

        return G;
    }

private:
    static std::random_device _rd;
    std::mt19937 _gen;
    node_type _node_count;
};

template <typename Graph>
std::random_device minimal_model<Graph>::_rd;

