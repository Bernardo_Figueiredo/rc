// vim: ts=8 sw=4 sts=4 expandtab

#pragma once
#include <cmath>
#include <random>

#include "assert/assert.hpp"
#include "models/model.hpp"

template <typename Graph>
struct watts_strogatz : public model<Graph> {
    typedef typename Graph::node_type node_type;

    watts_strogatz(node_type node_count, node_type num_jumps, double change_edge_p)
        : _gen(_rd())
        , _node_dist(0, node_count - 1)
        , _change_edge_p_dist(0, 1)
        , _node_count(node_count)
        , _num_jumps(num_jumps)
        , _change_edge_p(change_edge_p)
    {
        ASSERT_MSG(num_jumps >= 0 && num_jumps <= node_count, "num_jumps=" << num_jumps << "  node_count=" << node_count);
        ASSERT(node_count >= 0);
        ASSERT_MSG(change_edge_p >= 0.0 && change_edge_p <= 1.0, "change_edge_p=" << change_edge_p);
    }

    Graph operator()() override {
        // Reseed generator
        _gen.seed(_rd());

        Graph G(_node_count);

        /* Similar to creating a circular_lattice, but at every edge
           with probability p substitutes for a random edge */
        for (node_type node = 0; node < _node_count; ++node) {
            //we add the random edges after putting the "regular" edges, because if did it in a mixed way, when we would try to add a regular edge already added by a random edge
            // we wouldn't know what to do
            size_t num_random_edges = 0;
            for (node_type edge_distance = 1; edge_distance <= _num_jumps; ++edge_distance) {
                if (_change_edge_p_dist(_gen) < _change_edge_p) {
                    ++num_random_edges;
                } else {
                    _add_circular_lattice_edge(G, node, edge_distance);
                }
            }
            for(size_t num = 0; num < num_random_edges; ++num) {
                _add_random_edge(G, node);
            }
        }
        return G;
    }

private:
    static std::random_device _rd;
    std::mt19937 _gen;
    std::uniform_int_distribution<node_type> _node_dist;
    std::uniform_real_distribution<> _change_edge_p_dist;
    node_type _node_count;
    node_type _num_jumps;
    double _change_edge_p;

    //FIXME: copied from circular_lattice.hpp, maybe refactor later?
    void _add_circular_lattice_edge(Graph &g, node_type node, node_type edge_distance) {
        if (node + edge_distance >= _node_count) {
            //if the neighbour wraps around
            this->_add_edge(g, node, node + edge_distance - _node_count);
        } else {
            //if node + edge_distance doesn't wrap around
            this->_add_edge(g, node, node + edge_distance);
        }
    }

    void _add_random_edge(Graph &g, node_type node) {
            node_type neighbour;

            // Select a random node to add edge to
            do {
                neighbour=_node_dist(_gen);
            } while(this->_has_edge_or_self_edge(g, node, neighbour));
            this->_add_edge(g, node, neighbour);
    }
};

template <typename Graph>
std::random_device watts_strogatz<Graph>::_rd;
