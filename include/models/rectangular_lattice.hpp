#pragma once

#include "assert/assert.hpp"
#include "models/model.hpp"


/*
 * num_lines * num_columns nodes must fit in node_type
 *
 * node connections:
 *   nodes are connected to their immediate neighbours,
 *   nodes wrap so nodes in line 0 connect to nodes in line num_lines - 1
 *   if the graph is directed only the edges from a node to the node on it's left and to the node on top of it are made
 *
 * node numbering scheme:
 *   line*num_columns + column
 *
 * e.g.
 *   rectangular_lattice(2, 4)
 *   0 - 1 - 2 - 3
 *   |   |   |   |
 *   4 - 5 - 6 - 7
 *
 *   rectangular_lattice(4,1)
 *   0
 *   |
 *   1
 *   |
 *   2
 *   |
 *   3
 *
 */

template <typename Graph>
class rectangular_lattice : public model<Graph> {
    private:
        typedef typename Graph::node_type node_type;
        node_type _num_lines;
        node_type _num_columns;

        const node_type _node(node_type line, node_type column) const {
            return line*_num_columns + column;
        }

    public:
        rectangular_lattice(node_type num_lines, node_type num_columns) : _num_lines (num_lines), _num_columns(num_columns) {}

        Graph operator()() override {
	        Graph graph(_num_lines * _num_columns);

            /*
             * It's sufficient to add, for each node, when possible, and undirected edge to the neighbours:
             *   in the line above and with the same column
             *   in the columns to the left with the same line
             */
	        for(node_type line (0); line < _num_lines; line++) {
	            for(node_type column(0); column < _num_columns; column++) {
                    if (line != 0) {
                        graph.add_edge(_node(line, column), _node(line - 1, column));
                    } else if (_num_lines > 2 || (Graph::directed && _num_lines == 2)) { 
                        //Edge case. If the case above was >= 2 on undirected graphs there would be repeated edges, but == 2 case must be handled in the directed case
                        graph.add_edge(_node(line, column), _node(_num_lines - 1, column));
                    } //else _num_lines == 1 and line == 0, so we don't wrap to ourselves

                    if (column != 0) {
                        graph.add_edge(_node(line, column), _node(line, column - 1));
                    } else if (_num_columns > 2 || (Graph::directed && _num_columns == 2)) {
                        //Edge case. If the case above was >= 2 on undirected graphs there would be repeated edges, but == 2 case must be handled in the directed case
                        graph.add_edge(_node(line, column), _node(line, _num_columns -1));
                    } //else _num_columns == 1 and columns == 1, so we don't wrap to ourselves
	            }
	        }

            return graph;
        }
};
