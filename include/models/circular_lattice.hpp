// vim: ts=8 sts=4 sw=4 expandtab
#pragma once

#include "assert/assert.hpp"
#include "models/model.hpp"

template <typename Graph>
class circular_lattice : public model<Graph> {
    private:
        typedef typename Graph::node_type node_type;
        node_type _node_count;
        node_type _num_jumps;


    public:

    circular_lattice(node_type node_count, node_type num_jumps) : _node_count {node_count}, _num_jumps {num_jumps} {}

    Graph operator()() override {
        //Avoids edge case of generation algorithm when _num_jumps == _node_count/2
        //_node_count + 1 so we have the ceiling when the number is odd
        ASSERT(_num_jumps < (_node_count + 1)/2);

        Graph g(_node_count);

        //For each node we only add the edges to the right. When all the nodes do this all the expected edges will be in the graph
        //If we added the edges to the right and to the left for each node, there would be duplicate edges 
        for (node_type node = 0; node < _node_count; ++node) {
            for (node_type edge_distance = 1; edge_distance <= _num_jumps; ++edge_distance) {
                if (node + edge_distance >= _node_count) {
                    //if the neighbour wraps around
                    this->_add_edge(g, node, node + edge_distance - _node_count);
                } else {
                    //if node + edge_distance doesn't wrap around
                    this->_add_edge(g, node, node + edge_distance);
                }
            }
        }
        return g;
    }
};
