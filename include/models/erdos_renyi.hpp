// -*- vim: sw=4 expandtab

#pragma once

#include <iostream>
#include <random>

#include "assert/assert.hpp"
#include "models/model.hpp"

template <typename Graph>
struct erdos_renyi : public model<Graph> {
    typedef typename Graph::node_type node_type;

    erdos_renyi(node_type node_count, double p)
        : _gen(_rd())
        , _edge_dist(node_count*(node_count-1)/2, p)
        , _node_dist(0, node_count-1)
        , _node_count(node_count)
    {
        ASSERT_MSG(_node_count >= 0, "_node_count: " << _node_count);
        ASSERT_MSG(p >= 0.0 && p <= 1.0, "p_: " << p);
    }

    virtual Graph operator()() {
        // Reseed our generator
        _gen.seed(_rd());
        Graph G(_node_count);

        node_type edge_count = _edge_dist(_gen);

        /* For every possible pair of nodes
           adds the edge between them with probability p
        */
        while (G.edge_count() < edge_count) {
            auto u = _node_dist(_gen);
            auto v = _node_dist(_gen);
            if (!this->_has_edge_or_self_edge(G, u, v)) {
                G.add_edge(u, v);
            }
        }

        return G;
    }

private:
    static std::random_device _rd;
    std::mt19937 _gen;
    std::binomial_distribution<node_type> _edge_dist;
    std::uniform_int_distribution<node_type> _node_dist;
    node_type _node_count;
};

template <typename Graph>
std::random_device erdos_renyi<Graph>::_rd;
