// vim: ts=8 sw=4 sts=4 expandtab
#pragma once

#include <type_traits>

#include "graph/types.hpp"

/*
 * Cheat to make(in a simple way, hopefully) a rule that all models must only use dynamic_graphs, due to operations incompatibility.
 * All models should inherit from model.
 *
 * e.g.
 *
 * #include "model.hpp"
 *
 * template<typename Graph>
 * class circular_lattice : model<graph> {
 * ...
 * };
 *
 */ 
template<typename Graph>
struct model {
    static_assert(std::is_base_of<dynamic_graph,
        typename std::remove_cv<
            typename std::remove_reference<
                typename std::remove_pointer<
                    typename std::remove_all_extents<
                        Graph
                    >::type
                >::type
            >::type
        >::type
    >::value, "models must use dynamic_graphs");

protected:

    typedef typename Graph::node_type node_type;

    bool _has_edge_or_self_edge(const Graph &g, node_type u, node_type v) const {
        if (u == v) {
            return true;
        } else {
            return g.has_edge(u,v);
        }
    }

    void _add_edge(Graph& g, node_type u, node_type v) const {
        g.add_edge(u, v);
        if(Graph::directed) {
            g.add_edge(v, u);
        }
    }

public:

    virtual Graph operator()() = 0;

    virtual ~model() {}
};

