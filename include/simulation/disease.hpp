#include <iostream>
#include <cstdlib>
#include <ctime>
#include <random>
#include <cmath>
#include <algorithm>
#include <memory>
#include "assert/assert.hpp"

enum class node_status {SUSCEPTIBLE, INFECTED, RESISTANT, IMMUNE};

template <typename Graph>
class disease_model {
    public:
        typedef typename Graph::node_type node_type;

    private:

        static std::random_device _rd;
        std::mt19937 _gen;
        std::uniform_int_distribution<> _node_dist;
        std::uniform_real_distribution<> _real_dist;

        std::unique_ptr<node_type []> _interact_with_some_array;
        const Graph & _g;
        node_type _interactions_per_node;
        double _infection_rate;
        double _resistant_rate;
        double _susceptible_rate;
        double _infected_to_susceptible_rate;
        std::vector<node_status> _cur_node_status;
        std::vector<node_status> _new_node_status;

        // check in CUR_node_status
        bool _is_susceptible(node_type node) {return _cur_node_status[node] == node_status::SUSCEPTIBLE;}
        bool _is_infected(node_type node)    {return _cur_node_status[node] == node_status::INFECTED;}
        bool _is_resistant(node_type node)   {return _cur_node_status[node] == node_status::RESISTANT;}
        bool _is_immune(node_type node)      {return _cur_node_status[node] == node_status::IMMUNE;}

        //Updates the NEW_node_status
        void _susceptible(node_type node) {_new_node_status[node] = node_status::SUSCEPTIBLE;}
        //implemented "bussiness" rule here that immune/resistant can't be infected
        void _infected(node_type node)    { ASSERT(!_is_immune(node)); ASSERT(!_is_resistant(node)); _new_node_status[node] = node_status::INFECTED;}
        void _resistant(node_type node)   { ASSERT(_is_infected(node));  _new_node_status[node] = node_status::RESISTANT;}
        void _immune(node_type node)      {_new_node_status[node] = node_status::IMMUNE;}

        //*************************** susceptible -> infected *********************************************
        
        void _susceptible_to_infected_step() {
            if (_infection_rate == 0) {
                return;
            }
            
            for(node_type node=0; node< _g.node_count(); ++node){
                if (!_is_susceptible(node)) {
                    continue;
                }

                for (auto neighbour : _g.neighbours_of(node)) {
                    if(!_is_infected(neighbour)) {
                        continue;
                    }

                    // is infected
                    
                    if(_real_dist(_gen) < _infection_rate) {
                        _infected(node);
                        break; // the node is now infected so we can carry on to the next
                    }
                    break; //bus hypothesis so we only test for an infected_neighbour
                }
            }
        }

        //*************************** infected -> resistant **********************************************

        void _infected_to_resistant_step() {
            if(_resistant_rate == 0) {
                return;
            }

            for(node_type node=0; node< _g.node_count(); ++node){
                if (_is_infected(node) && _real_dist(_gen) < _resistant_rate) {
                    _resistant(node);
                }
            }
        }


        //*************************** resistant -> susceptible *******************************************

        void _resistant_to_susceptible_step() {
            if(_susceptible_rate == 0) {
                return;
            }

            for(node_type node=0; node< _g.node_count(); ++node){
                if (_is_resistant(node) && _real_dist(_gen) < _susceptible_rate) {
                    _susceptible(node);
                }
            }
        }

        //*************************** infected -> susceptible **********************************************

        void _infected_to_susceptible_step() {
            if(_infected_to_susceptible_rate == 0) {
                return;
            }

            for(node_type node=0; node< _g.node_count(); ++node){
                if (_is_infected(node) && _real_dist(_gen) < _infected_to_susceptible_rate) {
                    _susceptible(node);
                }
            }
        }


    public:
        disease_model(const Graph &g, node_type num_initial_infected_nodes, node_type interactions_per_node,
                double infection_rate, double resistant_rate, double susceptible_rate, double infected_to_susceptible_rate, const std::vector<node_type> &immune_nodes) :
            _gen(_rd()), _node_dist(0, g.node_count() -1), _real_dist(0,1), _interact_with_some_array {new node_type[interactions_per_node]},
            _g(g), _interactions_per_node(interactions_per_node),
            _infection_rate(infection_rate), _resistant_rate(resistant_rate), _susceptible_rate(susceptible_rate), _infected_to_susceptible_rate (infected_to_susceptible_rate),
            _cur_node_status(g.node_count(), node_status::SUSCEPTIBLE), _new_node_status(g.node_count(), node_status::SUSCEPTIBLE) {

                ASSERT(immune_nodes.size() <= g.node_count());
                ASSERT(num_initial_infected_nodes <= g.node_count());
                ASSERT(g.node_count() - immune_nodes.size() >= num_initial_infected_nodes); //Done this was to avoid overflow

                //immunize nodes
                for (auto node : immune_nodes) {
                    _immune(node);
                    _cur_node_status[node] = node_status::IMMUNE;
                }

                //Randomly infect num_initial_infected_nodes
                for (size_t iter = 0; iter <  num_initial_infected_nodes; ++iter) {
                    node_type node;
                    do {
                        node = _node_dist(_gen);
                    } while (_is_infected(node) || _is_immune(node)); //can't infect immune nodes or infected nodes. no resistant nodes yet
                    _infected(node);
                    _cur_node_status[node] = node_status::INFECTED;
                }
            }

        void do_time_steps(size_t num_time_steps) {
            for (size_t step = 0; step < num_time_steps; ++step) {
                do_time_step();
            }
        }



        void do_time_step() {
             //we don't need to copy the cur_node_status to new_node_status because at the beginning they're the same
             // and at each do_time step new_node_status is modified and the copied to cur_node_status, so they're always equal before/after do_time_step (not during)
             _susceptible_to_infected_step();
             _infected_to_resistant_step();
             _infected_to_susceptible_step();
             _resistant_to_susceptible_step();

            _cur_node_status = _new_node_status;
        }


        // check in CUR_node_status
        const std::vector<node_status> &infections() {return _cur_node_status;}
        node_status status(node_type node) {return _cur_node_status[node];}
};

template <typename Graph>
std::random_device disease_model<Graph>::_rd;

//*************************** Vaccination strategies *********************************************

#define NODE_TYPE typename Graph::node_type

template <typename Graph>
std::vector<NODE_TYPE> random_vaccination(const Graph &g,double vaccination_prob){

    std::vector<NODE_TYPE> immune_nodes;
    static std::random_device rd;
    std::mt19937 generator(rd());
    std::uniform_real_distribution<> real_dist(0,1);

    for(NODE_TYPE node=0; node < g.node_count(); ++node){
        if (real_dist(generator) < vaccination_prob){
            immune_nodes.push_back(node);
        }
    }
    return immune_nodes;
}

template <typename Graph>
std::vector<NODE_TYPE> vaccination_without_global_knowledge(const Graph &g,double prob_group_zero, size_t num_leaps){
    std::vector<NODE_TYPE> immune_nodes;
    static std::random_device rd;
    std::mt19937 generator(rd());
    std::uniform_real_distribution<> real_dist(0,1);

    for(NODE_TYPE node=0; node < g.node_count(); ++node){
        if (real_dist(generator) < prob_group_zero){
            NODE_TYPE cur_node = node; //cur_node from leap
            for (size_t leap = 0; leap < num_leaps; ++leap){
                auto &neighbours = g.neighbours_of(cur_node);
                if (neighbours.size() == 0) {
                    break;
                }
                NODE_TYPE neighbour = neighbours[real_dist(generator) * neighbours.size()];
                immune_nodes.push_back(neighbour);
                cur_node = neighbour;
            }
        }
    }
    return immune_nodes;
}

#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wstrict-overflow"
// Targeted vaccination strategy from Pastor-Satorras & Vespignani, 2002
template <typename Graph>
std::vector<NODE_TYPE> targeted_vaccination(const Graph &g,double vaccinated_fraction){
    std::vector<NODE_TYPE> immune_nodes;
    NODE_TYPE vaccinated_count = floor(vaccinated_fraction * g.node_count());

    std::vector<std::pair<NODE_TYPE, NODE_TYPE >> node_neigh_size;
    for( NODE_TYPE node = 0; node < g.node_count(); ++node ){
        node_neigh_size.push_back( {node, g.neighbours_of(node).size()} );
    }

    auto node_neigh_size_sorter = [&node_neigh_size] (const std::pair<NODE_TYPE, NODE_TYPE> &left, const std::pair<NODE_TYPE, NODE_TYPE> &right) -> bool { return left.second > right.second; }; // > because we want desc ordering
    std::sort(node_neigh_size.begin(), node_neigh_size.end(), node_neigh_size_sorter);

    for(NODE_TYPE node=0; node < vaccinated_count; ++node ){
        immune_nodes.push_back(node_neigh_size[node].first);
    }

    return immune_nodes;
}
#pragma GCC diagnostic pop



template <typename Graph>
disease_model<Graph> si_model(const Graph &g, NODE_TYPE num_initial_infected_nodes, NODE_TYPE interactions_per_node, double infection_rate) {
    return disease_model<Graph>(g, num_initial_infected_nodes, interactions_per_node, infection_rate, 0, 0, 0, {});
}
template <typename Graph>
disease_model<Graph> sis_model(const Graph &g, NODE_TYPE num_initial_infected_nodes, NODE_TYPE interactions_per_node, double infection_rate, double susceptible_rate) {
    return disease_model<Graph>(g, num_initial_infected_nodes, interactions_per_node, infection_rate, 0, 0, susceptible_rate, {});
}
template <typename Graph>
disease_model<Graph> sir_model(const Graph &g, NODE_TYPE num_initial_infected_nodes, NODE_TYPE interactions_per_node, double infection_rate, double resistant_rate) {
    return disease_model<Graph>(g, num_initial_infected_nodes, interactions_per_node, infection_rate, resistant_rate, 0, 0, {});
}
template <typename Graph>
disease_model<Graph> sirs_model(const Graph &g, NODE_TYPE num_initial_infected_nodes, NODE_TYPE interactions_per_node, double infection_rate, double resistant_rate, double susceptible_rate) {
    return disease_model<Graph>(g, num_initial_infected_nodes, interactions_per_node, infection_rate, resistant_rate, susceptible_rate, 0, {});
}
template <typename Graph>
disease_model<Graph> vaccination_model
   (const Graph &g, NODE_TYPE num_initial_infected_nodes, NODE_TYPE interactions_per_node, double infection_rate, double resistant_rate, double susceptible_rate, const std::vector<NODE_TYPE> &immune_nodes) {
    return disease_model<Graph>(g, num_initial_infected_nodes, interactions_per_node, infection_rate, resistant_rate, susceptible_rate, immune_nodes);
}
template <typename Graph>
disease_model<Graph> sis_random_vaccination_model(const Graph &g, NODE_TYPE num_initial_infected_nodes, NODE_TYPE interactions_per_node, double infection_rate, double infected_to_susceptible_rate, double vaccination_prob) {
    std::vector<NODE_TYPE> immune_nodes = random_vaccination(g, vaccination_prob);
    return disease_model<Graph>(g, num_initial_infected_nodes, interactions_per_node, infection_rate, 0, 0, infected_to_susceptible_rate, immune_nodes);
}
template <typename Graph>
disease_model<Graph> sis_targeted_vaccination_model(const Graph &g, NODE_TYPE num_initial_infected_nodes, NODE_TYPE interactions_per_node, double infection_rate, double infected_to_susceptible_rate, double vaccinated_fraction) {
    std::vector<NODE_TYPE> immune_nodes = targeted_vaccination(g, vaccinated_fraction);
    return disease_model<Graph>(g, num_initial_infected_nodes, interactions_per_node, infection_rate, 0, 0, infected_to_susceptible_rate, immune_nodes);
}
template <typename Graph>
disease_model<Graph> sis_vaccination_without_global_knowledge_model(const Graph &g, NODE_TYPE num_initial_infected_nodes, NODE_TYPE interactions_per_node, double infection_rate, double infected_to_susceptible_rate, double prob_group_zero, size_t num_leaps) {
    std::vector<NODE_TYPE> immune_nodes = vaccination_without_global_knowledge(g, prob_group_zero, num_leaps);
    return disease_model<Graph>(g, num_initial_infected_nodes, interactions_per_node, infection_rate, 0, 0, infected_to_susceptible_rate, immune_nodes);
}
#undef NODE_TYPE
