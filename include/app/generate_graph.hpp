#include "graph/graph.hpp"
#include "models/barabasi_albert.hpp"
#include "models/circular_lattice.hpp"
#include "models/erdos_renyi.hpp"
#include "models/minimal_model.hpp"
#include "models/watts_strogatz.hpp"
#include "models/rectangular_lattice.hpp"
#include "experimental/clique.hpp"


#include <sstream>
#include <functional>

template <typename Graph, typename Model>
void on_model_parse(int argc, char **argv, Model m);

std::string model_usage(void) {
    return std::string {
        "Available models:\n"
        "  barabasi_albert       FINAL_NODE_COUNT    START_NODE_COUNT    EDGES_PER_ITER  \n"
        "  circular_lattice      NODE_COUNT          MAX_JUMPS                           \n"
        "  clique                NODE_COUNT                                              \n"
        "  erdos_renyi           NODE_COUNT          EDGE_PROB                           \n"
        "  minimal_model         NODE_COUNT                                              \n"
        "  rectangular_lattice   NUM_LINES           NUM_COLUMNS                         \n"
        "  watts_strogatz        NODE_COUNT          MAX_JUMPS           CHANGE_PROB     \n"
    };
}

//TODO: FIXME: use argument reading library
template <typename Graph, typename Model, typename Arg1>
void parse_model_vararg(int argc, char **argv, std::function<void(void)> on_error) {
    if (argc < 1) {
        on_error();
    }

    std::stringstream ss;
    ss << argv[0];

    argc--;
    argv++;

    Arg1 arg1;
    if (ss >> arg1) {
        on_model_parse<Graph, Model>(argc, argv, Model (arg1));
    } else {
        on_error();
    }
}

template <typename Graph, typename Model, typename Arg1, typename Arg2>
void parse_model_vararg(int argc, char **argv, std::function<void(void)> on_error) {
    if (argc < 2) {
        on_error();
    }

    std::stringstream ss;
    ss << argv[0] << " " << argv[1];

    argc -= 2;
    argv += 2;

    Arg1 arg1;
    Arg2 arg2;
    if ((ss >> arg1) && (ss >> arg2)) {
        on_model_parse<Graph, Model>(argc, argv, Model (arg1, arg2));
    } else {
        on_error();
    }
}

template <typename Graph, typename Model, typename Arg1, typename Arg2, typename Arg3>
void parse_model_vararg(int argc, char **argv, std::function<void(void)> on_error) {
    if (argc < 3) {
        on_error();
    }

    std::stringstream ss;
    ss << argv[0] << " " << argv[1] << " " << argv[2];

    argc -= 3;
    argv += 3;

    Arg1 arg1;
    Arg2 arg2;
    Arg3 arg3;
    if ((ss >> arg1) && (ss >> arg2) && (ss >>arg3)) {
        on_model_parse<Graph, Model>(argc, argv, Model (arg1, arg2, arg3));
    } else {
        on_error();
    }
}


/* 
 * GEN(a, arg1) generates:
 *   function gen_a(int argc, char **argv, std::function<void(void)> on_error) that forwards the call to gen_arg with the same number of arguments(minus one, the name)
 *
 */
#define GEN(model_name, ...) \
    template<typename Graph> \
    void parse_ ## model_name (int argc, char **argv, std::function<void(void)> on_error) { \
        parse_model_vararg<Graph, model_name <Graph>, __VA_ARGS__>(argc, argv, on_error); \
    }

//Just the model name and it's arguments (don't forget to include the model)
GEN(barabasi_albert, size_t, size_t, size_t)
GEN(circular_lattice, size_t, size_t)
GEN(erdos_renyi, size_t, double)
GEN(minimal_model, size_t)
GEN(rectangular_lattice, size_t, size_t)
GEN(watts_strogatz, size_t, size_t, double)

void parse_clique(int argc, char **argv, std::function<void(void)> on_error) {
    parse_model_vararg<clique::Graph, clique, size_t>(argc, argv, on_error);
}
#undef GEN




template <typename Graph>
void parse_graph_model(int argc, char** argv, std::function<void(void)> on_error) {

    std::string model(argv[0]);
    --argc, argv++;

    if        (model == "barabasi_albert")     { parse_barabasi_albert<Graph>(argc, argv, on_error);} 
    else if   (model == "circular_lattice")    { parse_circular_lattice<Graph>(argc, argv, on_error);} 
    else if   (model == "clique")              { parse_clique(argc, argv, on_error);} 
    else if   (model == "erdos_renyi")         { parse_erdos_renyi<Graph>(argc, argv, on_error);} 
    else if   (model == "minimal_model")       { parse_minimal_model<Graph>(argc, argv, on_error);} 
    else if   (model == "rectangular_lattice") { parse_rectangular_lattice<Graph>(argc, argv, on_error);} 
    else if   (model == "watts_strogatz")      { parse_watts_strogatz<Graph>(argc, argv, on_error);} 
    else                                       { on_error();}
}
