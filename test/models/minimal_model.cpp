// vim: ts=8 sw=4 sts=4 expandtab

#include "graph/graph.hpp"
#include "models/minimal_model.hpp"
#include <gtest/gtest.h>

#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wstrict-overflow"

template <typename Graph, typename NodeType = typename Graph::node_type>
void test_node_count(NodeType node_count) {
    minimal_model<Graph> gen(node_count);
    auto g = gen();

    ASSERT_EQ(g.node_count(), node_count);

    for (NodeType i = 0; i < node_count; ++i) {
        ASSERT_GE(g.neighbours_of(i).size(), 2);
    }
}

TEST(MinimalModelU, MinDegree) {
    test_node_count<undirected_graph<int>>(10);
    test_node_count<undirected_graph<int>>(100);
    test_node_count<undirected_graph<int>>(1000);
}

#pragma GCC diagnostic pop
