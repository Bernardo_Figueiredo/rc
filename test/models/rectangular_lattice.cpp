#include "models/rectangular_lattice.hpp"
#include "graph/graph.hpp"

#include <vector>
#include <unordered_set>
#include <gtest/gtest.h>


template<typename Graph>
void test_expected_neighbours(std::vector<std::unordered_set<size_t>> expected_neighbours, typename Graph::node_type num_lines, typename Graph::node_type num_columns) {
    Graph g = rectangular_lattice<Graph> {num_lines, num_columns} ();

    for(size_t node(0); node < expected_neighbours.size(); node++) {
        EXPECT_EQ(expected_neighbours[node], g.neighbours_of(node));
    }
}

/*
 *  1x1
 */
TEST(RectangularLattice, DirectedRectangularLattice1x1) {
    std::vector<std::unordered_set<size_t>> expected_neighbours = { {} };
    test_expected_neighbours<directed_graph<size_t>>(expected_neighbours, 1, 1);
}
TEST(RectangularLattice, UndirectedRectangularLattice1x1) {
    std::vector<std::unordered_set<size_t>> expected_neighbours = { {} };
    test_expected_neighbours<undirected_graph<size_t>>(expected_neighbours, 1, 1);
}

/*
 *  2x2
 */
TEST(RectangularLattice, DirectedRectangularLattice2x2) {
    std::vector<std::unordered_set<size_t>> expected_neighbours = { {1, 2}, {0, 3},
                                                                    {0, 3}, {1, 2}};
    test_expected_neighbours<directed_graph<size_t>>(expected_neighbours, 2, 2);
}
TEST(RectangularLattice, UndirectedRectangularLattice2x2) {
    std::vector<std::unordered_set<size_t>> expected_neighbours = { {1, 2}, {0, 3},
                                                                    {0, 3}, {1, 2}};
    test_expected_neighbours<undirected_graph<size_t>>(expected_neighbours, 2, 2);
}

/*
 *  1x4
 */
TEST(RectangularLattice, DirectedRectangularLattice1x4) {
    std::vector<std::unordered_set<size_t>> expected_neighbours = { {3}, {0}, {1}, {2} };
    test_expected_neighbours<directed_graph<size_t>>(expected_neighbours, 1, 4);
}
TEST(RectangularLattice, UndirectedRectangularLattice1x4) {
    std::vector<std::unordered_set<size_t>> expected_neighbours = { {1, 3}, {0, 2}, {1, 3}, {0, 2} };
    test_expected_neighbours<undirected_graph<size_t>>(expected_neighbours, 1, 4);
}
/*
 *  4x1
 */
TEST(RectangularLattice, DirectedRectangularLattice4x1) {
    std::vector<std::unordered_set<size_t>> expected_neighbours = { {3}, {0}, {1}, {2} };
    test_expected_neighbours<directed_graph<size_t>>(expected_neighbours, 4, 1);
}
TEST(RectangularLattice, UndirectedRectangularLattice4x1) {
    std::vector<std::unordered_set<size_t>> expected_neighbours = { {1, 3}, {0, 2}, {1, 3}, {0, 2} };
    test_expected_neighbours<undirected_graph<size_t>>(expected_neighbours, 4, 1);
}

/*
 *  2x3
 */
TEST(RectangularLattice, DirectedRectangularLattice2x3) {
    std::vector<std::unordered_set<size_t>> expected_neighbours = { {2, 3}, {0, 4}, {1, 5},
                                                                    {0, 5}, {1, 3}, {2, 4}};
    test_expected_neighbours<directed_graph<size_t>>(expected_neighbours, 2, 3);
}
TEST(RectangularLattice, UndirectedRectangularLattice2x3) {
    std::vector<std::unordered_set<size_t>> expected_neighbours = { {1, 2, 3}, {0, 2, 4}, {0, 1, 5},
                                                                    {0, 4, 5}, {1, 3, 5}, {2, 3, 4}};
    test_expected_neighbours<undirected_graph<size_t>>(expected_neighbours, 2, 3);
}
/*
 *  3x2
 */
TEST(RectangularLattice, DirectedRectangularLattice3x2) {
    std::vector<std::unordered_set<size_t>> expected_neighbours = { {1, 4}, {0, 5},
                                                                    {0, 3}, {1, 2},
                                                                    {2, 5}, {3, 4}};
    test_expected_neighbours<directed_graph<size_t>>(expected_neighbours, 3, 2);
}
TEST(RectangularLattice, UndirectedRectangularLattice3x2) {
    std::vector<std::unordered_set<size_t>> expected_neighbours = {  {1, 2, 4}, {0, 3, 5},
                                                                     {0, 3, 4}, {1, 2, 5},
                                                                     {0, 2, 5}, {1, 3, 4}};
    test_expected_neighbours<undirected_graph<size_t>>(expected_neighbours, 3, 2);
}

/*
 *  3x3
 */
TEST(RectangularLattice, DirectedRectangularLattice3x3) {
    std::vector<std::unordered_set<size_t>> expected_neighbours = { {2, 6}, {0, 7}, {1, 8},
                                                                    {0, 5}, {1, 3}, {2, 4},
                                                                    {3, 8}, {4, 6}, {5, 7}};
    test_expected_neighbours<directed_graph<size_t>>(expected_neighbours, 3, 3);
}
TEST(RectangularLattice, UndirectedRectangularLattice3x3) {
    std::vector<std::unordered_set<size_t>> expected_neighbours = { {1, 2, 3, 6}, {0, 2, 4, 7}, {0, 1, 5, 8},
                                                                    {0, 4, 5, 6}, {1, 3, 5, 7}, {2, 3, 4, 8},
                                                                    {0, 3, 7, 8}, {1, 4, 6, 8}, {2, 5, 6, 7}};
    test_expected_neighbours<undirected_graph<size_t>>(expected_neighbours, 3, 3);
}
