#include "graph/graph.hpp"
#include "models/watts_strogatz.hpp"

#include <gtest/gtest.h>

directed_graph<size_t> get_watts_strogatz(const size_t node_count, const size_t jumps,const double p) {
    watts_strogatz<directed_graph<size_t>> ws {node_count, jumps, p};
    return ws();
}

TEST(WattsStrogatz, Success10NodesWithJump3WithP0) {
  auto G = get_watts_strogatz(10, 3,0);
  std::unordered_set<size_t> neighbours;

  neighbours = { 7, 8, 9, /* 0,*/ 1, 2, 3 }; EXPECT_EQ(G.neighbours_of(0), neighbours);
  neighbours = { 8, 9, 0, /* 1,*/ 2, 3, 4 }; EXPECT_EQ(G.neighbours_of(1), neighbours);
  neighbours = { 9, 0, 1, /* 2,*/ 3, 4, 5 }; EXPECT_EQ(G.neighbours_of(2), neighbours);
  neighbours = { 0, 1, 2, /* 3,*/ 4, 5, 6 }; EXPECT_EQ(G.neighbours_of(3), neighbours);
  neighbours = { 1, 2, 3, /* 4,*/ 5, 6, 7 }; EXPECT_EQ(G.neighbours_of(4), neighbours);
  neighbours = { 2, 3, 4, /* 5,*/ 6, 7, 8 }; EXPECT_EQ(G.neighbours_of(5), neighbours);
  neighbours = { 3, 4, 5, /* 6,*/ 7, 8, 9 }; EXPECT_EQ(G.neighbours_of(6), neighbours);
  neighbours = { 4, 5, 6, /* 7,*/ 8, 9, 0 }; EXPECT_EQ(G.neighbours_of(7), neighbours);
  neighbours = { 5, 6, 7, /* 8,*/ 9, 0, 1 }; EXPECT_EQ(G.neighbours_of(8), neighbours);
  neighbours = { 6, 7, 8, /* 9,*/ 0, 1, 2 }; EXPECT_EQ(G.neighbours_of(9), neighbours);
}
