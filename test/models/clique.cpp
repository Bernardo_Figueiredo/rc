#include "models/clique.hpp"
#include "graph/graph.hpp"

#include <vector>
#include <unordered_set>
#include <gtest/gtest.h>


template<typename Graph>
void test_clique(std::vector<std::unordered_set<typename Graph::node_type >> expected_neighbours) {
    const size_t clique_size {expected_neighbours.size()};

    clique<Graph> cl {clique_size};
    Graph graph {cl()};

    for(typename Graph::node_type node (0); node < expected_neighbours.size(); node++) {
        EXPECT_EQ(expected_neighbours[node], graph.neighbours_of(node));
    }
}

template<typename Graph>
void test_clique1() {
    std::vector<std::unordered_set<typename Graph::node_type>> expected_neighbours = {{}};
    test_clique<Graph>(expected_neighbours);
}
TEST(Clique, UndirectedClique1) {test_clique1<undirected_graph<size_t>>();}
TEST(Clique, DirectedClique1) {test_clique1<directed_graph<size_t>>();}

template<typename Graph>
void test_clique2() {
    std::vector<std::unordered_set<typename Graph::node_type>> expected_neighbours = {{1},
                                                                                      {0}};
    test_clique<Graph>(expected_neighbours);
}
TEST(Clique, UndirectedClique2) {test_clique2<undirected_graph<size_t>>();}
TEST(Clique, DirectedClique2) {test_clique2<directed_graph<size_t>>();}

template<typename Graph>
void test_clique3() {
    std::vector<std::unordered_set<typename Graph::node_type>> expected_neighbours = {{  1,2},
                                                                                      {0,  2},
                                                                                      {0,1  }};
    test_clique<Graph>(expected_neighbours);
}
TEST(Clique, UndirectedClique3) {test_clique3<undirected_graph<size_t>>();}
TEST(Clique, DirectedClique3) {test_clique3<directed_graph<size_t>>();}

template<typename Graph>
void test_clique4() {
    std::vector<std::unordered_set<typename Graph::node_type>> expected_neighbours = {{  1,2,3},
                                                                                      {0,  2,3},
                                                                                      {0,1,  3},
                                                                                      {0,1,2  }};
    test_clique<Graph>(expected_neighbours);
}
TEST(Clique, UndirectedClique4) {test_clique4<undirected_graph<size_t>>();}
TEST(Clique, DirectedClique4) {test_clique4<directed_graph<size_t>>();}

template<typename Graph>
void test_clique5() {
    std::vector<std::unordered_set<typename Graph::node_type>> expected_neighbours = {{  1,2,3,4},
                                                                                      {0,  2,3,4},
                                                                                      {0,1,  3,4},
                                                                                      {0,1,2  ,4},
                                                                                      {0,1,2,3  }};
    test_clique<Graph>(expected_neighbours);
}
TEST(Clique, UndirectedClique5) {test_clique5<undirected_graph<size_t>>();}
TEST(Clique, DirectedClique5) {test_clique5<directed_graph<size_t>>();}


