// vim: ts=8 sw=4 sts=4 expandtab

#include "graph/graph.hpp"
#include "models/circular_lattice.hpp"

#include <gtest/gtest.h>

template <typename Graph>
Graph get_circular_lattice(const size_t node_count, const size_t jumps) {
    circular_lattice<Graph> cl {node_count, jumps};
    return cl();
}

template<typename Graph>
void test_expected_neighbours(std::vector<std::unordered_set<size_t>> expected_neighbours, const Graph &graph) {
    for(size_t node(0); node < expected_neighbours.size(); node++) {
        EXPECT_EQ(expected_neighbours[node], graph.neighbours_of(node));
    }
}

template <typename Graph>
void test_circular_lattice_3_0() {
    auto G = get_circular_lattice<Graph>(3, 0);
    std::vector<std::unordered_set<size_t>> expected_neighbours = {{ /* 0 */},
                                                                   { /* 1 */},
                                                                   { /* 2 */}};
    test_expected_neighbours<Graph>(expected_neighbours, G);
}
TEST(CircularLattice, UndirectedSuccess3NodesWithJump0) {test_circular_lattice_3_0<undirected_graph<size_t>>();}
TEST(CircularLattice, DirectedSuccess3NodesWithJump0)   {test_circular_lattice_3_0<directed_graph<size_t>>();}


template <typename Graph>
void test_circular_lattice_3_1() {
    auto G = get_circular_lattice<Graph>(3, 1);

    std::vector<std::unordered_set<size_t>> expected_neighbours = {{ 2, /* 0,*/ 1 },
                                                                   { 0, /* 1,*/ 2 },
                                                                   { 1, /* 2,*/ 0 }};
    test_expected_neighbours<Graph>(expected_neighbours, G);
}
TEST(CircularLattice, UndirectedSuccess3NodesWithJump1) {test_circular_lattice_3_1<undirected_graph<size_t>>();}
TEST(CircularLattice, DirectedSuccess3NodesWithJump1)   {test_circular_lattice_3_1<directed_graph<size_t>>();}

template <typename Graph>
void test_circular_lattice_4_1() {
  auto G = get_circular_lattice<Graph>(4, 1);

    std::vector<std::unordered_set<size_t>> expected_neighbours = {{ 3, /* 0,*/ 1 },
                                                                   { 0, /* 1,*/ 2 },
                                                                   { 1, /* 2,*/ 3 },
                                                                   { 2, /* 3,*/ 0 }};
    test_expected_neighbours<Graph>(expected_neighbours, G);
}
TEST(CircularLattice, UndirectedSuccess4NodesWithJump1) {test_circular_lattice_4_1<undirected_graph<size_t>>();}
TEST(CircularLattice, DirectedSuccess4NodesWithJump1)   {test_circular_lattice_4_1<directed_graph<size_t>>();}

template <typename Graph>
void test_circular_lattice_5_2() {
  auto G = get_circular_lattice<Graph>(5, 2);
    std::vector<std::unordered_set<size_t>> expected_neighbours = {{ 3, 4, /* 0,*/ 1, 2 },
                                                                   { 4, 0, /* 1,*/ 2, 3 },
                                                                   { 0, 1, /* 2,*/ 3, 4 },
                                                                   { 1, 2, /* 3,*/ 4, 0 },
                                                                   { 2, 3, /* 4,*/ 0, 1 }};
    test_expected_neighbours<Graph>(expected_neighbours, G);
}
TEST(CircularLattice, UndirectedSuccess5NodesWithJump2) {test_circular_lattice_5_2<undirected_graph<size_t>>();}
TEST(CircularLattice, DirectedSuccess5NodesWithJump2)   {test_circular_lattice_5_2<directed_graph<size_t>>();}

template <typename Graph>
void test_circular_lattice_10_0() {
    auto G = get_circular_lattice<Graph>(10, 0);

    std::vector<std::unordered_set<size_t>> expected_neighbours = {{ /* 0,*/ },
                                                                   { /* 1,*/ },
                                                                   { /* 2,*/ },
                                                                   { /* 3,*/ },
                                                                   { /* 4,*/ },
                                                                   { /* 5,*/ },
                                                                   { /* 6,*/ },
                                                                   { /* 7,*/ },
                                                                   { /* 8,*/ },
                                                                   { /* 9,*/ }};
    test_expected_neighbours<Graph>(expected_neighbours, G);
}
TEST(CircularLattice, UndirectedSuccess10NodesWithJump0) {test_circular_lattice_10_0<undirected_graph<size_t>>();}
TEST(CircularLattice, DirectedSuccess10NodesWithJump0)   {test_circular_lattice_10_0<directed_graph<size_t>>();}


template <typename Graph>
void test_circular_lattice_10_3() {
    auto G = get_circular_lattice<Graph>(10, 3);

    std::vector<std::unordered_set<size_t>> expected_neighbours = {{ 7, 8, 9, /* 0,*/ 1, 2, 3 },
                                                                   { 8, 9, 0, /* 1,*/ 2, 3, 4 },
                                                                   { 9, 0, 1, /* 2,*/ 3, 4, 5 },
                                                                   { 0, 1, 2, /* 3,*/ 4, 5, 6 },
                                                                   { 1, 2, 3, /* 4,*/ 5, 6, 7 },
                                                                   { 2, 3, 4, /* 5,*/ 6, 7, 8 },
                                                                   { 3, 4, 5, /* 6,*/ 7, 8, 9 },
                                                                   { 4, 5, 6, /* 7,*/ 8, 9, 0 },
                                                                   { 5, 6, 7, /* 8,*/ 9, 0, 1 },
                                                                   { 6, 7, 8, /* 9,*/ 0, 1, 2 }};
    test_expected_neighbours<Graph>(expected_neighbours, G);
}
TEST(CircularLattice, UndirectedSuccess10NodesWithJump3) {test_circular_lattice_10_3<undirected_graph<size_t>>();}
TEST(CircularLattice, DirectedSuccess10NodesWithJump3)   {test_circular_lattice_10_3<directed_graph<size_t>>();}
