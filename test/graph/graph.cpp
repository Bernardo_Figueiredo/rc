// vim: ts=8 sw=4 sts=4 expandtab

#include "graph/graph.hpp"
#include "models/erdos_renyi.hpp"

#include <gtest/gtest.h>

TEST(UndirectedGraph, Counts) {
  undirected_graph<int> G(10);

  ASSERT_EQ(G.node_count(), 10);
  ASSERT_EQ(G.edge_count(), 0);

  G.add_edge(0, 1);
  ASSERT_EQ(G.node_count(), 10);
  ASSERT_EQ(G.edge_count(), 1);
}

TEST(DirectedGraph, Counts) {
  directed_graph<int> G(10);

  ASSERT_EQ(G.node_count(), 10);
  ASSERT_EQ(G.edge_count(), 0);

  G.add_edge(0, 1);
  ASSERT_EQ(G.node_count(), 10);
  ASSERT_EQ(G.edge_count(), 1);

  G.add_edge(1, 0);
  ASSERT_EQ(G.node_count(), 10);
  ASSERT_EQ(G.edge_count(), 2);
}

