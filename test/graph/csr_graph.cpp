#include "graph/csr_graph.hpp"
#include "models/clique.hpp"
#include <gtest/gtest.h>

#include <unordered_set>

template <typename T>
std::unordered_set<T> uset_from_neighbour_list(const typename csr_graph<T>::neighbour_list &neighbours) {
    std::unordered_set<T> uset;
    for (auto element : neighbours) {
        uset.insert(element);
    }

    return uset;
}

void test_clique(const csr_graph<size_t> &g) {
    size_t n {g.node_count()};

    //Check has_edges is correct
    for (size_t i {0}; i < n; i++) {
        for (size_t k{0}; k < n; k++) {
            if (i != k) {
                EXPECT_TRUE(g.has_edge(i, k));
            } else {
                EXPECT_FALSE(g.has_edge(i,k));
            }
        }
    }

    //check neighbours_of is correct
    for (size_t i {0}; i < n; i ++) {
        std::unordered_set<size_t> expected_neighbours;
        for (size_t k{0}; k < n; k++) {
            if (i != k) {
                expected_neighbours.insert(k);
            }
        }
        EXPECT_EQ(expected_neighbours, uset_from_neighbour_list<size_t>(g.neighbours_of(i)) );
    }
}

void test_clique_of(size_t n) {
    clique<undirected_graph<size_t>> gen(n);
    csr_graph<size_t> g(gen());

    test_clique(g);
}


TEST(csrgraph, CliqueOf2) {test_clique_of(2);}
TEST(csrgraph, CliqueOf3) {test_clique_of(3);}
TEST(csrgraph, CliqueOf4) {test_clique_of(4);}
TEST(csrgraph, CliqueOf10) {test_clique_of(10);}

/*
 * Tests that check that skipping nodes fills the _edge_index vector properly
 */

TEST(csrgraph, FirstNotConnected) {
    csr_graph<size_t> g(3, {
        { 1, 2 },
        { 2, 1 },
    });

    EXPECT_FALSE(g.has_edge(0,0));
    EXPECT_FALSE(g.has_edge(0,1));
    EXPECT_FALSE(g.has_edge(0,2));
    EXPECT_FALSE(g.has_edge(1,0));
    EXPECT_FALSE(g.has_edge(1,1));
    EXPECT_TRUE(g.has_edge(1,2));
    EXPECT_FALSE(g.has_edge(2,0));
    EXPECT_TRUE(g.has_edge(2,1));
    EXPECT_FALSE(g.has_edge(2,2));


    EXPECT_EQ(std::unordered_set<size_t> {}, uset_from_neighbour_list<size_t>(g.neighbours_of(0)));
    EXPECT_EQ(std::unordered_set<size_t> {2}, uset_from_neighbour_list<size_t>(g.neighbours_of(1)));
    EXPECT_EQ(std::unordered_set<size_t> {1}, uset_from_neighbour_list<size_t>(g.neighbours_of(2)));
}

TEST(csrgraph, LastNotConnected) {
    csr_graph<size_t> g(3, {
        { 0, 1 },
        { 1, 0 },
    });

    EXPECT_FALSE(g.has_edge(0,0));
    EXPECT_TRUE(g.has_edge(0,1));
    EXPECT_FALSE(g.has_edge(0,2));
    EXPECT_TRUE(g.has_edge(1,0));
    EXPECT_FALSE(g.has_edge(1,1));
    EXPECT_FALSE(g.has_edge(1,2));
    EXPECT_FALSE(g.has_edge(2,0));
    EXPECT_FALSE(g.has_edge(2,1));
    EXPECT_FALSE(g.has_edge(2,2));


    EXPECT_EQ(std::unordered_set<size_t> {1}, uset_from_neighbour_list<size_t>(g.neighbours_of(0)));
    EXPECT_EQ(std::unordered_set<size_t> {0}, uset_from_neighbour_list<size_t>(g.neighbours_of(1)));
    EXPECT_EQ(std::unordered_set<size_t> {}, uset_from_neighbour_list<size_t>(g.neighbours_of(2)));
}

TEST(csrgraph, MiddleNotConnected) {
    csr_graph<size_t> g(3, {
        { 0, 2 },
        { 2, 0 },
    });

    EXPECT_FALSE(g.has_edge(0,0));
    EXPECT_FALSE(g.has_edge(0,1));
    EXPECT_TRUE(g.has_edge(0,2));
    EXPECT_FALSE(g.has_edge(1,0));
    EXPECT_FALSE(g.has_edge(1,1));
    EXPECT_FALSE(g.has_edge(1,2));
    EXPECT_TRUE(g.has_edge(2,0));
    EXPECT_FALSE(g.has_edge(2,1));
    EXPECT_FALSE(g.has_edge(2,2));

    EXPECT_EQ(std::unordered_set<size_t> {2}, uset_from_neighbour_list<size_t>(g.neighbours_of(0)));
    EXPECT_EQ(std::unordered_set<size_t> {}, uset_from_neighbour_list<size_t>(g.neighbours_of(1)));
    EXPECT_EQ(std::unordered_set<size_t> {0}, uset_from_neighbour_list<size_t>(g.neighbours_of(2)));
}

TEST(csrgraph, FirstSecondNotConnected) {
    csr_graph<size_t> g(4, {
        { 2, 3 },
        { 3, 2 },
    });

    EXPECT_FALSE(g.has_edge(0,0));
    EXPECT_FALSE(g.has_edge(0,1));
    EXPECT_FALSE(g.has_edge(0,2));
    EXPECT_FALSE(g.has_edge(0,3));
    EXPECT_FALSE(g.has_edge(1,0));
    EXPECT_FALSE(g.has_edge(1,1));
    EXPECT_FALSE(g.has_edge(1,2));
    EXPECT_FALSE(g.has_edge(1,3));
    EXPECT_FALSE(g.has_edge(2,0));
    EXPECT_FALSE(g.has_edge(2,1));
    EXPECT_FALSE(g.has_edge(2,2));
    EXPECT_TRUE(g.has_edge(2,3));
    EXPECT_FALSE(g.has_edge(3,0));
    EXPECT_FALSE(g.has_edge(3,1));
    EXPECT_TRUE(g.has_edge(3,2));
    EXPECT_FALSE(g.has_edge(3,3));

    EXPECT_EQ(std::unordered_set<size_t> {}, uset_from_neighbour_list<size_t>(g.neighbours_of(0)));
    EXPECT_EQ(std::unordered_set<size_t> {}, uset_from_neighbour_list<size_t>(g.neighbours_of(1)));
    EXPECT_EQ(std::unordered_set<size_t> {3}, uset_from_neighbour_list<size_t>(g.neighbours_of(2)));
    EXPECT_EQ(std::unordered_set<size_t> {2}, uset_from_neighbour_list<size_t>(g.neighbours_of(3)));
}

/*
 *  IO
 *
 */

//TODO: FIXME: Remove after add_edge fix
#include "graph/graph.hpp"
#include "models/clique.hpp"
#include <iostream>
#include <cstddef>
#include "utils/sgf.hpp"

//TODO: FIXME: not passing because the ograph is a graph and it's neighbour_of are unordered_sets and the test expects the set to be ordered
TEST(csrgraph, CsrGraphFromSGF) {
    clique<directed_graph<size_t>> cl (10);
    directed_graph<size_t> ograph {cl()};

    std::stringstream iostream {};
    to_sgf(ograph, iostream);

    csr_graph<size_t> igraph = csr_graph_from_sgf<size_t>(iostream).second;
    test_clique(igraph);
}
