#include "stats/average_path_length.hpp"
#include "models/clique.hpp"
#include "models/rectangular_lattice.hpp"
#include "graph/graph.hpp"

#include <gtest/gtest.h>

template<typename Model, typename Graph>
void test_model(Model &model, double expected_apl) {
    Graph graph {model()};
    average_path_length<Graph> apl;
    apl.feed(graph);
    
    EXPECT_DOUBLE_EQ(expected_apl, apl.value());
}

template<typename Graph>
void test_clique_of(typename Graph::node_type clique_size, double expected_apl) {
    clique<Graph> model (clique_size);
    test_model<clique<Graph>, Graph>(model, expected_apl);
}

template<typename Graph>
void test_rectangular_lattice_of(typename Graph::node_type num_lines, typename Graph::node_type num_columns, double expected_apl) {
    rectangular_lattice<Graph> model (num_lines, num_columns);
    test_model<rectangular_lattice<Graph>, Graph>(model, expected_apl);
}



TEST(AveragePathLength, UndirectedCliqueOf1) {test_clique_of<undirected_graph<size_t>>(1, 0);} //No paths exist
TEST(AveragePathLength, DirectedCliqueOf1)   {test_clique_of<directed_graph<size_t>>  (1, 0);} //Same as above
TEST(AveragePathLength, UndirectedCliqueOf2) {test_clique_of<undirected_graph<size_t>>(2, 1);} // Only (1,2) (2,1) exist. 1 + 1 / 2 = 1
TEST(AveragePathLength, DirectedCliqueOf2)   {test_clique_of<directed_graph<size_t>>  (2, 1);} // Same as above
TEST(AveragePathLength, UndirectedCliqueOf3) {test_clique_of<undirected_graph<size_t>>(3, 1);} // (1,2), (1,3) (2,1) (2,3) (3,1) (3,2) 6*1 / 6 = 1
TEST(AveragePathLength, DirectedCliqueOf3)   {test_clique_of<directed_graph<size_t>>  (3, 1);} // Same as above
TEST(AveragePathLength, UndirectedCliqueOf4) {test_clique_of<undirected_graph<size_t>>(4, 1);} // All cliques(except clique(1)) have average path length of 1 because they're fully connected....
TEST(AveragePathLength, DirectedCliqueOf4)   {test_clique_of<directed_graph<size_t>>  (4, 1);} // Same as above
TEST(AveragePathLength, UndirectedCliqueOf5) {test_clique_of<undirected_graph<size_t>>(5, 1);} // All cliques(except clique(1)) have average path length of 1 because they're fully connected....
TEST(AveragePathLength, DirectedCliqueOf5)   {test_clique_of<directed_graph<size_t>>  (5, 1);} // Same as above

TEST(AveragePathLength, UndirectedRectangularLattice1x1) {test_rectangular_lattice_of<undirected_graph<size_t>>(1,1,0);} // No paths exist
TEST(AveragePathLength, DirectedRectangularLattice1x1) {test_rectangular_lattice_of<undirected_graph<size_t>>(1,1,0);} // No paths exist
TEST(AveragePathLength, UndirectedRectangularLattice1x2) {test_rectangular_lattice_of<undirected_graph<size_t>>(1,2,1);} // (0,1) and (1,0), so 1 + 1 /2 = 1
TEST(AveragePathLength, DirectedRectangularLattice1x2) {test_rectangular_lattice_of<undirected_graph<size_t>>(1,2,1);} // (1,0), so 1 / 1 = 1
TEST(AveragePathLength, UndirectedRectangularLattice2x1) {test_rectangular_lattice_of<undirected_graph<size_t>>(2,1,1);} // (0,1) and (1,0), so 1 + 1 /2 = 1
TEST(AveragePathLength, DirectedRectangularLattice2x1) {test_rectangular_lattice_of<undirected_graph<size_t>>(2,1,1);} // (1,0), so 1 / 1 = 1
TEST(AveragePathLength, UndirectedRectangularLattice2x2) {test_rectangular_lattice_of<undirected_graph<size_t>>(2,2, 4.0/3.0);} // (0,1) (0,2) (0,1,2), this applies to all nodes, so  (4 * ( 1 + 1 + 2)) / (3 * 4)
TEST(AveragePathLength, DirectedRectangularLattice2x2) {test_rectangular_lattice_of<undirected_graph<size_t>>(2,2, 4.0/3.0);}   // (1,0) (2,0) (3,1,0)  (1 + 1 + 2) / 3
TEST(AveragePathLength, UndirectedRectangularLattice2x3) {test_rectangular_lattice_of<undirected_graph<size_t>>(2,3, 7.0/5.0);} /* (0,1) (0,2) (0,3) (0,1,4) (0,2,4) for the corner nodes
                                                                                                                                 * (1,0) (1,2) (1,4) (1,0,3) (1,2,4) for the middle nodes
                                                                                                                                 *   ((1 + 1 + 1 + 2 + 2) * 4)  + ((1 + 1 + 1 + 2 +2) * 2)     42   7
                                                                                                                                 * -------------------------------------------------------- = --- = --
                                                                                                                                 *                   (5 * 4) + (5 * 2)                         30   5
                                                                                                                                 */ 
TEST(AveragePathLength, DirectedRectangularLattice2x3) {test_rectangular_lattice_of<directed_graph<size_t>>(2,3, 9.0/5.0);} /*     Node 0: (0,2) (0,3) (0,2,1) (0,3,5) (0,2,1,4)
                                                                                                                                 * Node 1: (1,0) (1,4) (1,0,2) (1,0,3) (1,0,2,5)
                                                                                                                                 * Node 2: (2,1) (2,5) (2,1,0) (2,1,4) (2,1,0,3)
                                                                                                                                 * Node 3: see 0
                                                                                                                                 * Node 4: see 1
                                                                                                                                 * Node 5: yadda
                                                                                                                                 *
                                                                                                                                 * so  1 + 1 + 2 + 2 +3   9 
                                                                                                                                 *    ----------------- = --  
                                                                                                                                 *            5           5
                                                                                                                                 */ 
