
#include "algo/bfs.hpp"
#include "graph/graph.hpp"
#include "models/clique.hpp"
#include "models/rectangular_lattice.hpp"
#include "models/circular_lattice.hpp"

#include <gtest/gtest.h>
#include <vector>


TEST(bfs, CliqueOf5) {
    using Graph = undirected_graph<size_t>;

    clique<Graph> c {5};
    Graph clique = c();
    bfs<Graph>  bfs {clique};

    std::vector<size_t> expected_distance {};
    std::vector<size_t> expected_parents {};

    bfs.visit(0);
    expected_distance = {0, 1, 1, 1, 1}; EXPECT_EQ(expected_distance, bfs.distances());
    expected_parents =  {0, 0, 0, 0, 0}; EXPECT_EQ(expected_parents,  bfs.parents());

    bfs.visit(1);
    expected_distance = {1, 0, 1, 1, 1}; EXPECT_EQ(expected_distance, bfs.distances());
    expected_parents =  {1, 1, 1, 1, 1}; EXPECT_EQ(expected_parents,  bfs.parents());

    bfs.visit(2);
    expected_distance = {1, 1, 0, 1, 1}; EXPECT_EQ(expected_distance, bfs.distances());
    expected_parents =  {2, 2, 2, 2, 2}; EXPECT_EQ(expected_parents,  bfs.parents());

    bfs.visit(3);
    expected_distance = {1, 1, 1, 0, 1}; EXPECT_EQ(expected_distance, bfs.distances());
    expected_parents =  {3, 3, 3, 3, 3}; EXPECT_EQ(expected_parents,  bfs.parents());

    bfs.visit(4);
    expected_distance = {1, 1, 1, 1, 0}; EXPECT_EQ(expected_distance, bfs.distances());
    expected_parents =  {4, 4, 4, 4, 4}; EXPECT_EQ(expected_parents,  bfs.parents());
}

template <typename Graph>
void test_parenthood(const bfs<Graph>& bfs, std::vector<std::set<typename Graph::node_type>> expected_parenthood) {

    typename Graph::node_type node = 0;
    for (auto node_parenthood : expected_parenthood) {
        typename Graph::node_type bfs_parent = bfs.parents()[node];
        EXPECT_TRUE(node_parenthood.find(bfs_parent) != node_parenthood.end()); 
        ++node;
    }
}

TEST(bfs, DirectedRectangularLattice2x3) {
    using Graph = directed_graph<size_t>;

    rectangular_lattice<Graph> r{2,3};
    Graph rectangular_lattice = r();
    bfs<Graph> bfs {rectangular_lattice};

    std::vector<size_t> expected_distance {};

    bfs.visit(0);
    expected_distance = {0, 2, 1,
                         1, 3, 2};
    EXPECT_EQ(expected_distance, bfs.distances());
    test_parenthood<Graph>(bfs, { {0}, {2},    {0},
                                  {0}, {1, 5}, {2, 3}});

    bfs.visit(1);
    expected_distance = {1, 0, 2,
                         2, 1, 3};
    EXPECT_EQ(expected_distance, bfs.distances());
    test_parenthood<Graph>(bfs, { {1},    {1}, {0},
                                  {0, 4}, {1}, {2, 3}});

    bfs.visit(2);
    expected_distance = {2, 1, 0,
                         3, 2, 1};
    EXPECT_EQ(expected_distance, bfs.distances());
    test_parenthood<Graph>(bfs, { {1},    {2},    {2},
                                  {0, 4}, {1, 5}, {2}});

    bfs.visit(3);
    expected_distance = {1, 3, 2,
                         0, 2, 1};
    EXPECT_EQ(expected_distance, bfs.distances());
    test_parenthood<Graph>(bfs, { {3}, {2, 4}, {0, 5},
                                  {3}, {5},    {3}});

    bfs.visit(4);
    expected_distance = {2, 1, 3,
                         1, 0, 2};
    EXPECT_EQ(expected_distance, bfs.distances());
    test_parenthood<Graph>(bfs, { {1, 3}, {4}, {0, 5},
                                  {4},    {4}, {3}});

    bfs.visit(5);
    expected_distance = {3, 2, 1,
                         2, 1, 0};
    EXPECT_EQ(expected_distance, bfs.distances());
    test_parenthood<Graph>(bfs, { {1, 3}, {2, 4}, {5},
                                  {4},    {5},    {5}});
}




TEST(bfs, UndirectedRectangularLattice3x3) {
    using Graph = undirected_graph<size_t>;

    rectangular_lattice<Graph> r{3,3};
    Graph rectangular_lattice = r();
    bfs<Graph> bfs {rectangular_lattice};

    std::vector<size_t> expected_distance {};

    bfs.visit(0);
    expected_distance = {0, 1, 1,
                         1, 2, 2,
                         1, 2, 2};
    EXPECT_EQ(expected_distance, bfs.distances());
    test_parenthood<Graph>(bfs, { {0}, {0},    {0},
                                  {0}, {1, 3}, {2, 3},
                                  {0}, {1, 6}, {2, 5}});

    bfs.visit(1);
    expected_distance = {1, 0, 1,
                         2, 1, 2,
                         2, 1, 2};
    EXPECT_EQ(expected_distance, bfs.distances());
    test_parenthood<Graph>(bfs, { {1},    {1}, {1},
                                  {0, 4}, {1}, {2, 4},
                                  {0, 7}, {1}, {2, 7}});

    bfs.visit(2);
    expected_distance = {1, 1, 0,
                         2, 2, 1,
                         2, 2, 1};
    EXPECT_EQ(expected_distance, bfs.distances());
    test_parenthood<Graph>(bfs, { {2},    {2},    {2},
                                  {0, 5}, {1, 5}, {2},
                                  {0, 8}, {1, 8}, {2}});

    bfs.visit(3);
    expected_distance = {1, 2, 2,
                         0, 1, 1,
                         1, 2, 2};
    EXPECT_EQ(expected_distance, bfs.distances());
    test_parenthood<Graph>(bfs, { {3}, {0, 4}, {0, 5},
                                  {3}, {3},    {3},
                                  {3}, {4, 6}, {5, 6}});

    bfs.visit(4);
    expected_distance = {2, 1, 2,
                         1, 0, 1,
                         2, 1, 2};
    EXPECT_EQ(expected_distance, bfs.distances());
    test_parenthood<Graph>(bfs, { {1, 3}, {4}, {1, 5},
                                  {4},    {4}, {4},
                                  {3, 7}, {4}, {5, 7}});

    bfs.visit(5);
    expected_distance = {2, 2, 1,
                         1, 1, 0,
                         2, 2, 1};
    EXPECT_EQ(expected_distance, bfs.distances());
    test_parenthood<Graph>(bfs, { {2, 3}, {2, 4}, {5},
                                  {5},    {5},    {5},
                                  {3, 8}, {4, 8}, {5}});

    bfs.visit(6);
    expected_distance = {1, 2, 2,
                         1, 2, 2,
                         0, 1, 1};
    EXPECT_EQ(expected_distance, bfs.distances());
    test_parenthood<Graph>(bfs, { {6}, {0, 7}, {0, 8},
                                  {6}, {3, 7}, {3, 8},
                                  {6}, {6},    {6}});
    
    bfs.visit(7);
    expected_distance = {2, 1, 2,
                         2, 1, 2,
                         1, 0, 1};
    EXPECT_EQ(expected_distance, bfs.distances());
    test_parenthood<Graph>(bfs, { {1, 6}, {7}, {1, 8},
                                  {4, 6}, {7}, {3, 8},
                                  {7},    {7}, {7}});

    bfs.visit(8);
    expected_distance = {2, 2, 1,
                         2, 2, 1,
                         1, 1, 0};
    EXPECT_EQ(expected_distance, bfs.distances());
    test_parenthood<Graph>(bfs, { {2, 6}, {2, 7}, {8},
                                  {5, 6}, {5, 7}, {8},
                                  {8},    {8},    {8}});

}



TEST(bfs, CircularLattice7x2) {
    using Graph = undirected_graph<size_t>;

    circular_lattice<Graph> cl {7, 2};
    Graph graph = cl();
    bfs<Graph> bfs{graph};

    std::vector<size_t> expected_distance {};
    std::vector<size_t> expected_parents {};

    bfs.visit(0);
    expected_distance = {0, 1, 1, 2, 2, 1, 1}; EXPECT_EQ(expected_distance, bfs.distances());
    test_parenthood<Graph>(bfs, { {0}, {0}, {0}, {1, 2, 5}, {2, 5, 6}, {0}, {0}});
    

    bfs.visit(1);
    expected_distance = {1, 0, 1, 1, 2, 2, 1}; EXPECT_EQ(expected_distance, bfs.distances());
    test_parenthood<Graph>(bfs, { {1}, {1}, {1}, {1}, {2, 3, 6}, {0, 3, 6}, {1}});
   
    bfs.visit(2);
    expected_distance = {1, 1, 0, 1, 1, 2, 2}; EXPECT_EQ(expected_distance, bfs.distances());
    //TODO: check parents
 
    bfs.visit(3);
    expected_distance = {2, 1, 1, 0, 1, 1, 2}; EXPECT_EQ(expected_distance, bfs.distances());
    //TODO: check parents

    bfs.visit(4);
    expected_distance = {2, 2, 1, 1, 0, 1, 1}; EXPECT_EQ(expected_distance, bfs.distances());
    //TODO: check parents

    bfs.visit(5);
    expected_distance = {1, 2, 2, 1, 1, 0, 1}; EXPECT_EQ(expected_distance, bfs.distances());
    //TODO: check parents

    bfs.visit(6);
    expected_distance = {1, 1, 2, 2, 1, 1, 0}; EXPECT_EQ(expected_distance, bfs.distances());
    //TODO: check parents
}

TEST(bfs, CircularLattice7x1) {
    using Graph = undirected_graph<size_t>;
    circular_lattice<Graph> cl {7, 1};
    Graph graph = cl();
    bfs<Graph> bfs{graph};

    std::vector<size_t> expected_distance {};
    std::vector<size_t> expected_parents {};

    bfs.visit(0);
    expected_distance = {0, 1, 2, 3, 3, 2, 1}; EXPECT_EQ(expected_distance, bfs.distances());
    expected_parents =  {0, 0, 1, 2, 5, 6, 0}; EXPECT_EQ(expected_parents,  bfs.parents());

    bfs.visit(1);
    expected_distance = {1, 0, 1, 2, 3, 3, 2}; EXPECT_EQ(expected_distance, bfs.distances());
    expected_parents =  {1, 1, 1, 2, 3, 6, 0}; EXPECT_EQ(expected_parents,  bfs.parents());
   
    bfs.visit(2);
    expected_distance = {2, 1, 0, 1, 2, 3, 3}; EXPECT_EQ(expected_distance, bfs.distances());
    expected_parents =  {1, 2, 2, 2, 3, 4, 0}; EXPECT_EQ(expected_parents,  bfs.parents());
 
    bfs.visit(3);
    expected_distance = {3, 2, 1, 0, 1, 2, 3}; EXPECT_EQ(expected_distance, bfs.distances());
    expected_parents =  {1, 2, 3, 3, 3, 4, 5}; EXPECT_EQ(expected_parents,  bfs.parents());

    bfs.visit(4);
    expected_distance = {3, 3, 2, 1, 0, 1, 2}; EXPECT_EQ(expected_distance, bfs.distances());
    expected_parents =  {6, 2, 3, 4, 4, 4, 5}; EXPECT_EQ(expected_parents,  bfs.parents());

    bfs.visit(5);
    expected_distance = {2, 3, 3, 2, 1, 0, 1}; EXPECT_EQ(expected_distance, bfs.distances());
    expected_parents =  {6, 0, 3, 4, 5, 5, 5}; EXPECT_EQ(expected_parents,  bfs.parents());

    bfs.visit(6);
    expected_distance = {1, 2, 3, 3, 2, 1, 0}; EXPECT_EQ(expected_distance, bfs.distances());
    expected_parents =  {6, 0, 1, 4, 5, 6, 6}; EXPECT_EQ(expected_parents,  bfs.parents());
}

