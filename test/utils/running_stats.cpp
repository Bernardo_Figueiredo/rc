#include "utils/running_stats.hpp"
#include <gtest/gtest.h>
#include <cstdlib>
#include <ctime>

TEST(RunningStats, Initial) {
    running_stats stats;

    ASSERT_EQ(0, stats.count());
    ASSERT_DOUBLE_EQ(0.0, stats.mean());
    ASSERT_DOUBLE_EQ(0.0, stats.variance());
    ASSERT_DOUBLE_EQ(0.0, stats.stddev());
}

TEST(RunningStats, Count) {
    running_stats stats;

    stats.feed(1.0);
    ASSERT_EQ(1, stats.count());
    stats.feed(2.0);
    ASSERT_EQ(2, stats.count());
    stats.feed(3.0);
    ASSERT_EQ(3, stats.count());
    stats.feed(4.0);
    ASSERT_EQ(4, stats.count());
    stats.feed(5.0);
    ASSERT_EQ(5, stats.count());
}

TEST(RunningStats, Mean) {
    running_stats stats;

    stats.feed(1.0);
    ASSERT_DOUBLE_EQ(1.0, stats.mean());
    stats.feed(2.0);
    ASSERT_DOUBLE_EQ(1.5, stats.mean());
    stats.feed(3.0);
    ASSERT_DOUBLE_EQ(2.0, stats.mean());
    stats.feed(4.0);
    ASSERT_DOUBLE_EQ(2.5, stats.mean());
    stats.feed(5.0);
    ASSERT_DOUBLE_EQ(3.0, stats.mean());
}

TEST(RunningStats, VarianceAndStdDev) {
    running_stats stats;

    stats.feed(1.0);
    ASSERT_DOUBLE_EQ(0.0, stats.variance());
    ASSERT_DOUBLE_EQ(sqrt(stats.variance()), stats.stddev());
    stats.feed(2.0);
    ASSERT_DOUBLE_EQ(0.5, stats.variance());
    ASSERT_DOUBLE_EQ(sqrt(stats.variance()), stats.stddev());
    stats.feed(3.0);
    ASSERT_DOUBLE_EQ(1.0, stats.variance());
    ASSERT_DOUBLE_EQ(sqrt(stats.variance()), stats.stddev());
    stats.feed(4.0);
    ASSERT_DOUBLE_EQ(5.0/3.0, stats.variance());
    ASSERT_DOUBLE_EQ(sqrt(stats.variance()), stats.stddev());
    stats.feed(5.0);
    ASSERT_DOUBLE_EQ(10.0/4.0, stats.variance());
    ASSERT_DOUBLE_EQ(sqrt(stats.variance()), stats.stddev());
}
