\section{Representações de grafos}

Temos representações \emph{dinâmicas} (\code{graph}, \code{ra\_graph}), que suportam a adição e remoção (não implementada) de arcos;
\emph{estáticas} (\code{csr\_graph}), que utilizam menos memória mas são menos flexíveis, i.e.\ o custo de alterar o grafo é grande;
e \emph{implícitas} (\code{clique}), que não requerem uma representação para o grafo mas também não podem ser alteradas.

O caso de uso para representações dinâmicas é a geração de grafos e uso interactivo;
as representações estáticas são melhores para análise e serialização, pois são mais eficientes em termos de espaço.
As representações implícitas são bastante úteis quando o grafo é completamente determinístico e o custo de gerar o grafo é alto.
Um exemplo deste último caso é um clique, que requer tempo e memória proporcional a $O(n^{2})$, mas é tão simples que é mais barato gerar os resultados das consultas (e.g.\ consultar vizinhos) do que representar o grafo em memória.

Em nenhuma das implementações permitimos grafos pesados, ou multigrafos\cite{new10}.
Também não permitimos adição de nós após a criação dos grafos, sendo o número fixado na criação.
Todas as classes (excepto as representações implícitas) estão parametrizadas com um booleano que indica se é um grafo orientado ou não.

\subsection{graph}

Esta implementação utiliza \code{std::vector} com $n$ \code{std::unordered\_set} por nó para representar os vizinhos.
Permitir grafos pesados, seria uma adição simples: trocar \code{std::unordered\_set} por \code{std::unordered\_map}, em que os valores do mapa são os pesos dos arcos.

\subsubsection*{Complexidade:}
\begin{itemize}
  \item Inicialização: $\Theta(n)$. \\
  Um \code{std::vector} com $n$ elementos leva $\Theta(n)$ \cite[{[vector.cons]}/5]{c++11_draft}.
  Os elementos deste \emph{vector} são \code{std::unordered\_set}s, e cada um destes leva tempo constante a ser inicializado por omissão \cite[{[unord.set.cnstr]}/2]{c++11_draft}.
  
  \item Memória: $\Theta(n+m)$. \\
  O \emph{standard} do C++ deixa os requisitos de memória dependentes da implementação.
  No entanto, examinando a implementação (GCC 5.2.1), tanto o \code{std::unordered\_set} como o \code{std::vector} usam memória proporcional ao número de elementos.
  
  \item Adição de arco: $O(1)$ em média, $O(n)$ no pior caso. \\
  Aceder à vizinhança do nó é feito em tempo constante \cite[{[sequence.reqmts]}/16]{c++11_draft}.
  O custo de inserção num \code{std::unordered\_set} é $O(1)$ em média, e $O(n)$ no pior caso \cite[Table 103]{c++11_draft}.
  
  \item Existência de arco: $O(1)$ em média, $O(n)$ no pior caso. \\
  Aceder à vizinhança do nó é feito em tempo constante.
  A procura na vizinhança é feita em $O(1)$ em média, e $O(n)$ no pior caso \cite[Table 103]{c++11_draft}.
  
  \item Vizinhança de um nó: $O(1)$.
  Aceder à vizinhança do nó é feito em tempo constante.
\end{itemize}

\subsection{ra\_graph}

Implementação igual a \code{graph}, mas utilizando um \code{std::vector} para guardar os vizinhos em vez de um \code{std::unordered\_set}.
A vantagem desta implementação é que para grafos com $\langle k \rangle$ pequeno, é mais rápida (por exemplo a navegar a vizinhança) que o \code{graph}, pois a memória é contígua, logo não tem que dereferenciar ponteiros (excepto para aceder à $array$ de memória).
A desvantagem é que o custo da operação de teste de existência de é $O(n)$ no pior caso (\code{clique}), e tendo um custo médio mais elevado que o \code{graph} quando $\langle k \rangle$ se torna elevado.

Para a maior parte dos modelos usa-se parâmetros de tal modo a que $\langle k \rangle$ seja pequeno, logo isto é mais rápido que o \code{graph} no nosso caso em específico.

\subsection{csr\_graph}

Esta implementação estática usa duas \emph{arrays}, uma com os vizinhos de cada nó e outro com os índices de cada nó no \emph{array} de vizinhos.
O principal objectivo desta representação é a de reduzir a memória ocupada pelo grafo, de forma a permitir retirar resultados de grafos de maior dimensão em memória.

\subsubsection*{Complexidade:}
\begin{itemize}
    \item Inicialização: $O(n^{2} \log{n})$. \\
    Só pode ser construído a partir de um grafo já existente.
    Dado um grafo com $n$ nós e $m$ arcos, são alocadas duas \emph{arrays} de tamanho $n+1$ e $m$, respectivamente.
    As vizinhanças são ordenadas na \emph{array} de vizinhos, o que é $n$ vezes $O(n\log{n})$.
    A \emph{array} de índices tem como elemento na posição $i$ a soma parcial dos tamanhos das vizinhanças dos nós $0 \dotso i-1$, ou $0$ para a primeira posição.
    Preencher esta \emph{array} tem custo $O(n)$.
    
    \item Memória: $O(n+m)$. \\
    A \emph{array} de índices tem exactamente $n+1$ elementos, e a \emph{array} de vizinhos tem $m$.
    
    \item Adição de arco: não suportada.
    
    \item Existência de arco: $O(\log{n})$. \\
    Os vizinhos de um nó estão ordenados, por isso podemos utilizar uma pesquisa binária.
    O C++ implementa este algoritmo (como \code{std::binary\_search}) e garante tempo $O(\log{n})$ \cite[{[binary.search]}/3]{c++11_draft}.
    
    \item Vizinhança de um nó: $O(1)$. \\
    Retorna-se uma estrutura que tem dois ponteiros que funcionam como \emph{bounds} para iteração.
\end{itemize}

\subsection{clique\_graph}

Esta implementação lembra-se apenas do número de nós do clique, e consegue responder a todas as operações com isso.

\subsubsection*{Complexidade:}
\begin{itemize}
	\item Inicialização: $\Theta(1)$. \\
	Construção trivial.
	
	\item Memória: $\Theta(1)$. \\
	Apenas tem que guardar o número $n$ de nós no grafo.
	
	\item Existência de arco: $\Theta(1)$. \\
	Dados dois nós, basta verificar se são o mesmo. Se forem, o arco não existe pois não são permitidas \emph{multi-edges}, caso contrário existe.
	
	\item Vizinhança de um nó: $\Theta(1)$. \\
	Retorna-se uma estrutura iterável que gera números, por ordem, de $0 \dotso n-1$, saltando o próprio nó.
\end{itemize}
