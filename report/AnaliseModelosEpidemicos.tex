\section{Análise Modelos Epidémicos} \label{ref:analise_modelos_epidemicos}

Nesta secção, fazemos a verificação dos modelos epidémicos implementados.
Usamos como comparação as soluções das equações diferenciais que os modelam com a aproximação de homogeneidade.
Nesta aproximação, assume-se que todos os elementos da população podem interagir em cada iteração com qualquer outro elemento da população com igual probabilidade\cite{new10}.
Além desta aproximação, as equações diferenciais também assumem populações infinitas, portanto vamos ter desvios em relação ao valor esperado devido a efeitos da nossa população ser finita, que faz com que haja uma variação nos valores dos parâmetros dos modelos com o decorrer da simulação.
Um exemplo disto, é quando um nó infectado fica só com vizinhos já infectados, pois deixa de contribuir para infectar outros nós, havendo uma ``redução efectiva'' na fracção de infectados nas equações.

Como na nossa implementação percorremos todos os vizinhos, usar um clique para ``simular'' esta aproximação não é o mais correcto, porque em todas as iterações todos os nós susceptíveis vão interagir sempre com um nó infectado(porque todos os nós estão ligados a todos os outros e se há um infectado, então todos os nós vão interagir com ele), em vez de com um número fixo de vizinhos aleatórios (dos quais pode não estar nenhum infectado).
Deste modo decidimos usar o modelo  $\model{Watts-Strogatz}(50000, 2, 1)$, tal como no artigo \emph{Epidemic dynamics and endemic states in complex networks.} \cite{watts_strogratz_for_epidemics}. Assim há alguma aleatoriedade (apesar de ser "estática", no sentido em que vai sempre interagir com os mesmos vizinhos, para a simulação inteira), pedida pela aproximação usada, e também temos um número fixo médio de interacções, que é $\langle k \rangle=4$ (porque cada nó tem 4 vizinhos). 
Este modelo, mesmo com $p = 1$ não é equivalente a um a uma rede aleatória, pois a estrutura da rede mantém alguma memória da \model{Circular Lattice} inicial, todos os nós têm pelo menos grau 2. \cite{watts_strogratz_for_epidemics}
Não usámos \model{Erd\H{o}s-Rényi(n, p)}, porque o grafo resultante pode não ser ligado(mesmo na região crítica).

As figuras apresentadas são obtidas fazendo a média a cada iteração do valor obtido, incluindo o desvio padrão amostral. Em cada secção corre-se o modelo 10 vezes, sempre com os grafos diferentes, mas gerados do mesmo modelo.

Utiliza-se a notação $s(t)$, $i(t)$ e $r(t)$ para indicar, a fracção de nós susceptíveis, infectados e resistentes, respectivamente, na iteração $t$ do modelo. $i_{0}$ representa a fracção de infectados inicial.

Nesta secção começamos sempre as simulações com $i_{0}=500$ ($1\%$ da população).

\subsection{SI($\beta$)} 

Neste caso usámos $\beta=0.1$.

Os resultados estão apresentados na Figura \ref*{fig:si_model}. A linha verde escura apresenta o resultado teórico esperado\cite{barabasi16}, (\ref*{eq:si_infected}).

\begin{equation}
\label{eq:si_infected}
i(t) = \frac{i_{0} e^{\beta \langle k \rangle t}}{1 - i_{0} + i_{0} e^{\beta \langle k \rangle t}}, i_{0} = 0.1, \beta=0.1, \langle k \rangle = 4
\end{equation}

Os pontos verdes apresentam o resultado da simulação da fracção de nós infectados e a azul a fracção de nós susceptíveis.
A vermelho está a fracção de resistentes, a verde de infectados e a azul de susceptíveis.


\begin{figure}
	\begin{center}
		\includegraphics[scale=0.4]{graphics/si_model_500_4_0-1_watts_strogatz_50000_2_1.png}
		\caption{Evolução do modelo SI em \model{Watts-Strogatz(50000, 2, 1)}, $\beta=0.1$}
		\label{fig:si_model}
	\end{center}
\end{figure}

\subsection{SIS($\beta$, $\delta$)} \label{sec:analise_modelos_epidemicos_sis}

Neste caso usámos $\beta=0.2$, $delta=0.15$.
Os resultados estão apresentados na Figura \ref*{fig:sis_model}. A linha verde escura apresenta o resultado teórico esperado\cite{barabasi16}, (\ref*{eq:sis_infected}).

\begin{equation}
\label{eq:sis_infected}
i(t) = (1 - \frac{\delta}{\beta \langle k  \rangle}) \frac{C e^{(\beta \langle k  \rangle) - \delta) t}}{1 + C e^{(\beta \langle k  \rangle) - \delta) t}}, C=\frac{i_{0}}{1 - i_{0} - \frac{\delta}{\beta \langle k \rangle}}, i_{0} = 0.1, \delta=0.15, \beta=0.2, \langle k \rangle = 4
\end{equation}

Os pontos verdes apresentam o resultado da simulação da fracção de nós infectados e a azul a fracção de nós susceptíveis.
A vermelho está a fracção de resistentes, a verde de infectados e a azul de susceptíveis.

A fracção de infectados estacionários obtida, $i(\infty) \approx 0.55$. Está longe do valor teórico esperado\cite{barabasi16}, que é $i(\infty) = 1 - \frac{\delta}{\beta \langle k \rangle} \Bigr|_{\substack{\beta=0.2\\\delta=0.15\\\langle k \rangle = 4}} = 0.8125$. Se fixarmos $\beta$ e $\delta$, podemos concluir que o nosso $\langle k \rangle$ efectivo, para o $i(\infty)$ obtido é $\langle k \rangle = 1.(6)$. Esta diferença deve-se ao efeitos de que já falámos.

\begin{figure}
	\begin{center}
		\includegraphics[scale=0.4]{graphics/sis_model_500_4_0-2_0-15_watts_strogatz_50000_2_1.png}
		\caption{Evolução do modelo SIS em $\model{Watts-Strogatz}(50000, 2, 1)$, $\beta=0.2$, $\delta=0.15$}
		\label{fig:sis_model}
	\end{center}
\end{figure}

\subsection{SIR($\beta$, $\gamma$)}

Neste caso usámos $\beta=0.2$, $\gamma=0.05$.
O gráfico encontra-se semelhante a outros que encontrámos\cite{barabasi16}.
Não apresentamos as curvas esperadas por não termos encontrado soluções em forma fechada para elas.
Os resultados estão apresentados na Figura \ref*{fig:sir_model}.


\begin{figure}
	\begin{center}
		\includegraphics[scale=0.4]{graphics/sir_model_500_4_0-2_0-05_watts_strogatz_50000_2_1.png}
		\caption{Evolução do modelo SIR em \model{Watts-Strogatz(50000, 2, 1)}, $\beta=0.2$, $\gamma=0.05$}
		\label{fig:sir_model}
	\end{center}
\end{figure}
